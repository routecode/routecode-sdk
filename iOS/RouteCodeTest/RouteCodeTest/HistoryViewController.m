//
//  HistoryViewController.m
//  RouteCodeTest
//
//  Created by PLATFIELD INC.
//
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#import "HistoryViewController.h"
#import "Common.h"

@interface HistoryViewController ()
@property(weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation HistoryViewController

/**
 * アプリ起動後、の画面初描画時
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    DLog(@"--- Start ---")
}

// View が表示される直前に呼ばれるメソッド
// タブ等の切り替え等により、画面に表示されるたびに呼び出されます。
// タブが切り替わるたびに何度でも呼ばれます。
- (void)viewWillAppear:(BOOL)animated {
    DLog(@"-------------")
    [super viewWillAppear:animated];

    NSString *dns = nil;
    
  switch (ENVIRONMENT_MODE) {
        case EnvironmentModeEnumProduction:
          dns = DNS_PRODUCTION;
          break;
        case EnvironmentModeEnumStaging:
            dns = DNS_STAGING;
            break;
        case   EnvironmentModeEnumDevelop:
            dns = DNS_DEVELOP;
            break;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/get-receipt-data.php?uuid=%@",dns, self.uuid];

    DLog(@"urlString:%@", urlString)

    NSURLSession *urlSession;
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 5;
    sessionConfig.timeoutIntervalForResource = 20;

    // 処理を単純化させるためにdelegateは使用しませんでした。
    // UI関連の処理を想定していないため、delegateQueueは独自に作成しました。
    urlSession = [NSURLSession sessionWithConfiguration:sessionConfig
                                               delegate:nil
                                          delegateQueue:nil];

    NSLog(@"get start");

    NSMutableURLRequest *request = [NSMutableURLRequest new];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setValue:@"YKTHttpClient" forHTTPHeaderField:@"User-Agent"];
    [request setHTTPMethod:@"GET"];

    // 取得するデータサイズが小さいのでtaskはNSURLSessionDataTaskを使用
    NSURLSessionDataTask *dataTask
            = [urlSession dataTaskWithRequest:request
                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                NSHTTPURLResponse *httpUrlResponse = (NSHTTPURLResponse *) response;
                                DLog(@"Status code: %ld", httpUrlResponse.statusCode);

                                CFStringEncoding encoding = CFStringConvertIANACharSetNameToEncoding((CFStringRef) [response textEncodingName]);
                                NSString *body = [[NSString alloc] initWithData:data encoding:CFStringConvertEncodingToNSStringEncoding(encoding)];
                                NSData *restoreData = [[NSData alloc] initWithBase64EncodedString:body options:0];

                                NSString *restoreString = [[NSString alloc] initWithData:restoreData encoding:NSUTF8StringEncoding];
                                dispatch_async(
                                        dispatch_get_main_queue(),
                                        ^{
                                            self.textView.text = restoreString;
                                        }
                                );
                            }];

    [dataTask resume];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
