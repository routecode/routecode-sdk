//
//  UserInfoViewController.h
//  RouteCodeTest
//
//  Created by GoYamada on 2022/06/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoViewController : UIViewController
@property(nonatomic, strong) NSString *uuid;

@end

NS_ASSUME_NONNULL_END
