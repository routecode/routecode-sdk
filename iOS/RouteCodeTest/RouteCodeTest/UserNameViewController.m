//
//  UserNameViewController.m
//  RouteCodeTest
//
//  Created by GoYamada on 2022/07/21.
//
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#import "UserNameViewController.h"

#import "Common.h"

@interface UserNameViewController ()

@end

@implementation UserNameViewController
- (IBAction)buttonAction:(id)sender {

    // ユーザーデータ初期化API
    [RoutePay initialUuidServiceId:SERVICE_ID        // サービスID
                        merchantId:MERCHANT_ID       // 店舗ID
                   environmentMode:ENVIRONMENT_MODE  // サーバ環境
                       callSuccess:^(NSString *uuid, int userNo) {
        DLog("uuid[%@]",uuid)
        DLog("userNoStr[%d]",userNo)
       if ([self fileSave:WRITE_FILE_NAME_UUID data:uuid]) {
           NSString *userNoStr = [NSString stringWithFormat:@"%d", userNo];
           if ([self fileSave:WRITE_FILE_NAME_USER_NO data:userNoStr]) {
               DLog("保存成功")
               NSString *name = self.textField.text;
               [self postUserName:name uuid:uuid];

               [self.navigationController popViewControllerAnimated:YES];
           }
       }
    }
                        callFailed:^(int status, NSError *error) {
        
    }];


}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSString *date_converted;

    NSDate *date_source = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYYMMddhhmmss"];
    date_converted = [formatter stringFromDate:date_source];
    NSString *name = [NSString stringWithFormat:@"ユーザー%@", date_converted];
    [self.textField setText:name];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)fileSave:(NSString *)fileName data:(NSString *)data {
    // /Documentのパスの取得
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // ファイル名の作成
    NSString *filename = [paths[0] stringByAppendingPathComponent:fileName];
    NSError *error;
    // ファイルへの保存
    BOOL result = [data writeToFile:filename atomically:YES encoding:NSUTF8StringEncoding error:&error];

    return result;
}

- (void)postUserName:(NSString *)name uuid:(NSString *)uuid {

    NSString *dns = nil;

    switch (ENVIRONMENT_MODE) {
        case EnvironmentModeEnumProduction:
            dns = DNS_PRODUCTION;
            break;
        case EnvironmentModeEnumStaging:
            dns = DNS_STAGING;
            break;
        case EnvironmentModeEnumDevelop:
            dns = DNS_DEVELOP;
            break;
    }

    NSString *urlString = [NSString stringWithFormat:@"%@/set-user-data.php", dns];
    NSDictionary *params = @{
            @"name": name,
            @"uuid": uuid,
    };
    DLog(@"urlString:%@", urlString)

    // 連想配列として与えられたパラメータをクエリ文字列に変換する
    NSData *query = [self buildQueryWithDictionary:params];


    NSURLSession *urlSession;
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.timeoutIntervalForRequest = 5;
    sessionConfig.timeoutIntervalForResource = 20;

    // 処理を単純化させるためにdelegateは使用しませんでした。
    // UI関連の処理を想定していないため、delegateQueueは独自に作成しました。
    urlSession = [NSURLSession sessionWithConfiguration:sessionConfig
                                               delegate:nil
                                          delegateQueue:nil];

    NSLog(@"get start");

    NSMutableURLRequest *request = [NSMutableURLRequest new];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    [request setValue:@"iOS" forHTTPHeaderField:@"User-Agent"];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long) [query length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:query];

    // 取得するデータサイズが小さいのでtaskはNSURLSessionDataTaskを使用
    NSURLSessionDataTask *dataTask
            = [urlSession dataTaskWithRequest:request
                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                NSHTTPURLResponse *httpUrlResponse = (NSHTTPURLResponse *) response;
                                DLog(@"Status code: %ld", httpUrlResponse.statusCode);

                                CFStringEncoding encoding = CFStringConvertIANACharSetNameToEncoding((CFStringRef) [response textEncodingName]);
                                NSString *body = [[NSString alloc] initWithData:data encoding:CFStringConvertEncodingToNSStringEncoding(encoding)];
                                NSData *restoreData = [[NSData alloc] initWithBase64EncodedString:body options:0];

                                NSString *restoreString = [[NSString alloc] initWithData:restoreData encoding:NSUTF8StringEncoding];
                                dispatch_async(
                                        dispatch_get_main_queue(),
                                        ^{
                                        }
                                );
                            }];

    [dataTask resume];
}

// クエリ文字列を得る
- (NSData *)buildQueryWithDictionary:(NSDictionary *)params {
    // 連想配列のキーと値をそれぞれ URL エンコードし、 key=value の形で配列に追加していく
    NSMutableArray *parts = [NSMutableArray array];
    for (id key in params) {
        [parts addObject:[NSString stringWithFormat:@"%@=%@",
                                                    [self encodeURIComponent:(NSString *) key],
                                                    [self encodeURIComponent:(NSString *) [params objectForKey:key]]]];
    }

    // それぞれを & で結ぶ
    NSString *queryString = [parts componentsJoinedByString:@"&"];

    // NSURLRequest setHTTPBody: に渡せるよう NSData に変換する
    return [queryString dataUsingEncoding:NSUTF8StringEncoding];
}


// JavaScript の encodeURIComponent() 相当
- (NSString *)encodeURIComponent:(NSString *)string {
    return (__bridge_transfer NSString *) CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef) string, NULL, CFSTR(";:@&=+$,/?%#[]"), kCFStringEncodingUTF8);
}

@end
