//
//  Common.h
//  RouteCodeTest
//
//  Created by GoYamada on 2022/07/21.
//

#ifndef Common_h
#define Common_h
#import <RouteCode/RoutePay.h>

#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#define WRITE_FILE_NAME_UUID @"uuid"
#define WRITE_FILE_NAME_USER_NO @"user_no"
//#define ENVIRONMENT_MODE EnvironmentModeEnumProduction
#define ENVIRONMENT_MODE EnvironmentModeEnumStaging // <-ステージングです。こちらでテストして下さい。


#define DNS_PRODUCTION  @"https://sandbox.unid.net"
#define DNS_STAGING  @"https://stg-sandbox.unid.net"
#define DNS_DEVELOP  @"https://dev-sandbox.unid.net"

#define SERVICE_ID @"サービスIDを入れてください"
#define MERCHANT_ID @"店舗IDを入れてください"


#endif /* Common_h */
