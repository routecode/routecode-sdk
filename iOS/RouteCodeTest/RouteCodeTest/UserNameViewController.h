//
//  UserNameViewController.h
//  RouteCodeTest
//
//  Created by GoYamada on 2022/07/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserNameViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

NS_ASSUME_NONNULL_END
