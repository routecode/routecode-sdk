//
//  ViewController.m
//  RouteCodeTest
//
//  Created by PLATFIELD INC.
//

#import "ViewController.h"

#import "HistoryViewController.h"
#import "UserInfoViewController.h"

#import "Common.h"

@interface ViewController ()
@property(nonatomic) BOOL debugMode;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}

// View が表示される直前に呼ばれるメソッド
// タブ等の切り替え等により、画面に表示されるたびに呼び出されます。
// タブが切り替わるたびに何度でも呼ばれます。
- (void)viewWillAppear:(BOOL)animated {
    DLog(@"-------------")
    [super viewWillAppear:animated];
    



}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // Do any additional setup after loading the view.
    
    // あらかじめ保存しているファイルから取得
    NSString *uuid = [self fileRead:WRITE_FILE_NAME_UUID];

    if (uuid == nil) {
        [self performSegueWithIdentifier:@"userName" sender:self];
    }

}

- (IBAction)testButton:(id)sender {
    [self callCardRegister];
}

- (IBAction)testButton2:(id)sender {
    [self callPayment];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"History"]) {
        NSString *uuid = [self fileRead:WRITE_FILE_NAME_UUID];
        NSString *userNo = [self fileRead:WRITE_FILE_NAME_USER_NO];

        if (uuid != nil) {
            HistoryViewController *historyViewController = [segue destinationViewController];
            historyViewController.uuid = uuid;
        } else {
            DLog(@"データがありません")
        }

    }
}

- (BOOL)fileSave:(NSString *)fileName data:(NSString *)data {
    // /Documentのパスの取得
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // ファイル名の作成
    NSString *filename = [paths[0] stringByAppendingPathComponent:fileName];
    NSError *error;
    // ファイルへの保存
    BOOL result = [data writeToFile:filename atomically:YES encoding:NSUTF8StringEncoding error:&error];

    return result;
}

- (NSString *)fileRead:(NSString *)fileName {
    // /Documentのパスの取得
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // ファイル名の作成
    NSString *filename = [paths[0] stringByAppendingPathComponent:fileName];
    NSError *error;
    // ファルからの読込み
    NSString *content = [NSString stringWithContentsOfFile:filename
                                                  encoding:NSUTF8StringEncoding
                                                     error:&error];

    if (error != nil) {
        return nil;
    }

    return content;
}


// ユーザー情報取得
- (void)getCardInfo:(void (^)(NSString *uuid, NSInteger userNo, int status, NSError *error))responseBrock {
    // あらかじめ保存しているファイルから取得
    NSString *uuid = [self fileRead:WRITE_FILE_NAME_UUID];
    NSString *userNoStr = [self fileRead:WRITE_FILE_NAME_USER_NO];

    if (uuid == nil) {
        [self performSegueWithIdentifier:@"userName" sender:self];
    } else {
  
        NSInteger userNo = [userNoStr intValue];
        responseBrock(uuid,userNo, 0, nil);
    }
}

// カード登録
- (void)callCardRegister {

    // 自薦に必要なユーザー情報を取得
    [self getCardInfo:^(NSString *uuid, NSInteger userNo, int status, NSError *error) {

        if (status != 0) {
            UIAlertController *alertController
                    = [UIAlertController alertControllerWithTitle:@"エラー"
                                                          message:error.description
                                                   preferredStyle:UIAlertControllerStyleAlert];

            //下記のコードでボタンを追加します。また{}内に記述された処理がボタン押下時の処理なります。
            [alertController addAction:[UIAlertAction actionWithTitle:@"はい"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action) {
                                                                  //ボタンがタップされた際の処理
                                                              }]];

            //下記のコードでダイアログを表示
            [self presentViewController:alertController animated:YES completion:nil];

            return;

        } else {
            // カードのステータス
            if (uuid == nil) {
                UIAlertController *alertController
                        = [UIAlertController alertControllerWithTitle:@"エラー"
                                                              message:@"サービスID、店舗IDまたは、uuIDがセットされていません"
                                                       preferredStyle:UIAlertControllerStyleAlert];

                //下記のコードでボタンを追加します。また{}内に記述された処理がボタン押下時の処理なります。
                [alertController addAction:[UIAlertAction actionWithTitle:@"はい"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction *action) {
                                                                      //ボタンがタップされた際の処理
                                                                  }]];

                //下記のコードでダイアログを表示
                [self presentViewController:alertController animated:YES completion:nil];

                return;
            }

            [RoutePay callCardRegisterUuid:uuid
                                    userNo:userNo
                                viewController:self
                               environmentMode:ENVIRONMENT_MODE
                                   callSuccess:^(NSString *uuid, int userNo) {
                                       DLog(@"成功");
                                   }
                                    callFailed:^(int status, NSError *error2) {
                                        DLog(@"Error[%@]", error2.description)
                                    }];
        }
    }];
}

// 決済
- (void)callPayment {
    [self getCardInfo:^(NSString *uuid, NSInteger userNo, int status, NSError *error) {

        if (status != 0) {
            DLog(@"Error[%@]", error.description)
        } else {
            if (uuid == nil) {
                UIAlertController *alertController
                        = [UIAlertController alertControllerWithTitle:@"エラー"
                                                              message:@"カードが登録されていません"
                                                       preferredStyle:UIAlertControllerStyleAlert];

                //下記のコードでボタンを追加します。また{}内に記述された処理がボタン押下時の処理なります。
                [alertController addAction:[UIAlertAction actionWithTitle:@"はい"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction *action) {
                                                                      //ボタンがタップされた際の処理
                                                                  }]];

                //下記のコードでダイアログを表示します。
                [self presentViewController:alertController animated:YES completion:nil];

                return;
            }

            // 決済API
            [RoutePay callPaymentUuid:uuid
                               userNo:userNo
                           viewController:self
                          environmentMode:ENVIRONMENT_MODE
                              callSuccess:^(NSString *uuid, int userNo) {
                                  DLog(@"完了")

                              } callFailed:^(int status, NSError *error2) {
                        DLog(@"Error[%@]", error2.description)

                    }];

        }
    }];

}

@end
