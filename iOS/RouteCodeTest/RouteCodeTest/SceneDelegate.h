//
//  SceneDelegate.h
//  RouteCodeTest
//
//  Created by PLATFIELD INC.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

