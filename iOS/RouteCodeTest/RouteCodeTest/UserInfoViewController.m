//
//  UserInfoViewController.m
//  RouteCodeTest
//
//  Created by GoYamada on 2022/06/15.
//

#import "UserInfoViewController.h"
#import "Common.h"

@interface UserInfoViewController ()
@property(weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation UserInfoViewController

/**
 * アプリ起動後、の画面初描画時
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    DLog(@"--- Start ---")
}

- (void)viewWillAppear:(BOOL)animated {
    DLog(@"-------------")
    [super viewWillAppear:animated];

    NSString *userInfoText = [NSString stringWithFormat:
                              @""
//            @"serviceId: "  @"%@\n"
//            @"merchantId: " @"%@\n"
//            @"uuid: "       @"%@\n"
//            @"privateKeyClient:\n"
//            @"%@\n"
//            @"publicKeyServer:\n"
//            @"%@\n"
//            , self.userInfo.serviceId
//            , self.userInfo.merchantId
//            , self.userInfo.uuid
//            , self.userInfo.privateKeyClient
//            , self.userInfo.publicKeyServer
    ];

    dispatch_async(
            dispatch_get_main_queue(),
            ^{
                self.textView.text = userInfoText;
            }
    );

}
@end
