//
//  RoutePayTest.h
//  RouteCode
//
//  Created by PLATFIELD INC.
//

// テスト用



#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RoutePayTest : NSObject

//  ログが出力されます
+ (void) log:(NSString*) message;
//  Webブラウザが立ち上がります。
+ (void)openStoryBoard:(UIViewController *)viewController;
//  Webブラウザが立ち上がります。
+ (void)openStoryBoardNoNav:(UIViewController *)viewController;
@end

NS_ASSUME_NONNULL_END
