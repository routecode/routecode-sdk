//
//  RoutePay.h
//  RouteCode
//
//  Created by PLATFIELD INC.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <RouteCode/ResponseBlock.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, EnvironmentModeEnum) {
    EnvironmentModeEnumProduction = 0,
    EnvironmentModeEnumStaging = 1,
    EnvironmentModeEnumDevelop = 2,
};

/**
 * RoutePayメインメソッドクラス
 */
@interface RoutePay : NSObject

/**
 * ユーザー情報初期化(環境モード付き)
 *
 * @param[in] serviceId : サービスID
 * @param[in] merchantId : 店舗ID
 * @param[in] environmentMode : サーバー環境
 * @param[out] responseSuccessBlock : ユーザー情報生成レスポンス
 * @param[out] responseFailedBlock : エラーレスポンス
 */
+ (void)initialUuidServiceId:(NSString *)serviceId
                  merchantId:(NSString *)merchantId
             environmentMode:(EnvironmentModeEnum)environmentMode
                 callSuccess:(ResponseInitialUuidSuccessBlockType)responseSuccessBlock
                  callFailed:(ResponseInitialUuidFailedBlockType)responseFailedBlock;

/**
 *  @brief ユーザー情報初期化
 *
 * @param[in] serviceId : サービスID
 * @param[in] merchantId : 店舗ID
 * @param[out] responseSuccessBlock : ユーザー情報生成レスポンス
 * @param[out] responseFailedBlock : エラーレスポンス
 */
+ (void)initialUuidServiceId:(NSString *)serviceId
                  merchantId:(NSString *)merchantId
                 callSuccess:(ResponseInitialUuidSuccessBlockType)responseSuccessBlock
                  callFailed:(ResponseInitialUuidFailedBlockType)responseFailedBlock;

/**
 * @brief クレジットカード登録(環境モード付き)
 *
 * @param userNo : ユーザーNo
 * @param viewController : コールバック用ビューコントローラ
 * @param environmentMode : サーバー環境
 * @param responseCardRegistSuccessBlock : カード登録レスポンスブロックス
 * @param responseCardRegistFailedBlock : エラーレスポンス
 */
+ (void)callCardRegisterUuid:(NSString *)uuid
                      userNo:(NSInteger)userNo
              viewController:(UIViewController *)viewController
             environmentMode:(EnvironmentModeEnum)environmentMode
                 callSuccess:(ResponseCardRegistSuccessBlockType)responseCardRegistSuccessBlock
                  callFailed:(ResponseCardRegistFailedBlockType)responseCardRegistFailedBlock;

/**
 * @brief クレジットカード登録
 *
 * @param userNo : ユーザーNo
 * @param viewController : コールバック用ビューコントローラ
 * @param responseCardRegistSuccessBlock : カード登録レスポンスブロックス
 * @param responseCardRegistFailedBlock : エラーレスポンス
 */
+ (void)callCardRegisterUuid:(NSString *)uuid
                      userNo:(NSInteger)userNo
              viewController:(UIViewController *)viewController
                 callSuccess:(ResponseCardRegistSuccessBlockType)responseCardRegistSuccessBlock
                  callFailed:(ResponseCardRegistFailedBlockType)responseCardRegistFailedBlock;


/**
 * @brief 決済(環境モード付き)
 *
 * @param userNo :ユーザーNo
 * @param viewController : コールバック用ビューコントローラ
 * @param environmentMode : サーバー環境
 * @param responseBPaymentSuccessBlock : 決済レスポンスブロックス
 * @param responseBPaymentFailedBlock : エラーレスポンス
 */
+ (void)callPaymentUuid:(NSString *)uuid
                 userNo:(NSInteger)userNo
         viewController:(UIViewController *)viewController
        environmentMode:(EnvironmentModeEnum)environmentMode
            callSuccess:(ResponsePaymentSuccessBlockType)responseBPaymentSuccessBlock
             callFailed:(ResponsePaymentFailedBlockType)responseBPaymentFailedBlock;

/**
 * @brief 決済
 *
 * @param userNo : ユーザーNo
 * @param viewController : コールバック用ビューコントローラ
 * @param responseBPaymentSuccessBlock : 決済レスポンスブロックス
 * @param responseBPaymentFailedBlock : エラーレスポンス
 */
+ (void)callPaymentUuid:(NSString *)uuid
                 userNo:(NSInteger)userNo
         viewController:(UIViewController *)viewController
            callSuccess:(ResponsePaymentSuccessBlockType)responseBPaymentSuccessBlock
             callFailed:(ResponsePaymentFailedBlockType)responseBPaymentFailedBlock;

@end

NS_ASSUME_NONNULL_END
