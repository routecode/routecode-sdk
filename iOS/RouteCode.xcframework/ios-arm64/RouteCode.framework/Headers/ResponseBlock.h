//
//  ResponseBlock.h
//  RouteCode
//
//  Created by PLATFIELD INC.
//


#ifndef ResponseBlock_h
#define ResponseBlock_h

/***
 * ユーザー情報生成レスポンスブロックス
 */
typedef void  (^ResponseInitialUuidSuccessBlockType)(NSString *uuid, int userNo);
typedef void  (^ResponseInitialUuidFailedBlockType)(int status, NSError *error);

/***
 * カード登録レスポンスブロックス
 */
typedef void  (^ResponseCardRegistSuccessBlockType)(NSString *uuid, int userNo);
typedef void  (^ResponseCardRegistFailedBlockType)(int status, NSError *error);

/***
 * 決済レスポンスブロックス
 */
typedef void  (^ResponsePaymentSuccessBlockType)(NSString *uuid, int userNo);
typedef void  (^ResponsePaymentFailedBlockType)(int status, NSError *error);

#endif /* ResponseBrock_h */
