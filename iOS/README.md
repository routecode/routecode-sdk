# RouteCode sdk for ios

## フォルダ構成

```
.
├── README.md
├── RouteCode.xcframework   <- RouteCode　SDKです。
├── RouteCodeTest           <- Objective-C のサンプルアプリソース
│   ├── Podfile             <- CocoaPodsFのile
│   └── RouteCodeTest.xcworkspace
└── RouteCodeSwiftTest      <- Swift のサンプルアプリソース
    ├── Podfile             <- CocoaPodsFのile
    └── RouteCodeSwiftTest.xcworkspace 
```

## SDK 本体

「./RouteCode.xcframework」が本体となります。



「RouteCode sdk for ios」では、本件のフレームワーク以外に、OpenSSLのモジュールは別途インストールします。

CocoaPodsで、OpenSSL(Version 1.1.1)ライブラリとして、「[OpenSSL-Universal](https://cocoapods.org/pods/OpenSSL-Universal)」を使用しています。



### フレームワークの追加方法

CocoaPodsについては、[ここ](https://cocoapods.org/)を参考してください。

また、ライブラリは、「[OpenSSL-Universal](https://cocoapods.org/pods/OpenSSL-Universal)」からご確認ください。



プロジェクトに設定する

アプリ Taget > General > Framworks, Libraries, and Embedded Content
ここに xcframework をさきほどビルドしたところから Finder からドラッグドロップで設定します。

※ フレームワークはどこに置いて頂いても構いませんが、相対パスで追加されますので、プログラムフォルダを移動される場合は、フレームワーク自体も移動して下さい。



## サンプルアプリ

### Objective-C

「RouteCodeTest」に同梱しております。

### Swift

「RouteCodeSwiftTest」に同梱しております。



## ライセンスについて

本 SDK では、オープンソースを使用しております。

「/RouteCode.xcframework/ios-arm64/RouteCode.framework」内に
「License-xxxxxx.plist」で表示されているものは、ライセンス表記となります。

### NSData-Base64

https://github.com/l4u/NSData-Base64

