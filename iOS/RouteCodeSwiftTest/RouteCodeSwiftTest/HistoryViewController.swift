//
//  HistoryViewController.swift
//  RouteCodeSwiftTest
//
//  Created by GoYamada on 2022/06/15.
//

import UIKit
import RouteCode

class HistoryViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!

    var uuid: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        let dns: String

        switch (ENVIRONMENT_MODE) {
        case EnvironmentModeEnum.production:
            dns = DNS_PRODUCTION
            break;
        case EnvironmentModeEnum.staging:
            dns = DNS_STAGING
            break;
        case EnvironmentModeEnum.develop:
            dns = DNS_DEVELOP
            break;
        }

        let urlString = dns + "/get_receipt_data.php?uuid=" + uuid

//        let urlString: String = "https://dev-sandbox.unid.net/get_receipt_data.php?uuid=" + userInfo.uuid

        let url = URL(string: urlString)!  //URLを生成
        let request = URLRequest(url: url)               //Requestを生成
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            //非同期で通信を行う
            guard let data = data else {
                return
            }
            do {
                let encodedString: String? = String(data: data, encoding: .utf8)
                let restoreData = Data(base64Encoded: encodedString!)
                let restoreString = String(data: restoreData!, encoding: .utf8)

                DispatchQueue.main.async { [self] in

                    // メインスレッドで実行 UIの処理など
                    self.textView.text = restoreString ?? ""

                }
                print()
            } catch let error {
                print(error)
            }
        }
        task.resume()

    }

}
