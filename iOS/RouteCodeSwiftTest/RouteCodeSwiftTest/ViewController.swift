//
//  ViewController.swift
//  RouteCodeSwiftTest
//
//  Created by PLATFIELD INC.
//

//import RouteCode.RoutePayTest

import RouteCode
import UIKit

public func DLog(_ items: Any?,
                 file: String = #file,
                 line: Int = #line,
                 function: String = #function) {
    #if DEBUG
    let filename = URL(fileURLWithPath: file).lastPathComponent
    if let items = items {
        print("[\(filename) \(line):\(function)] : \(items)")
    } else {
        print("[\(filename) \(line):\(function)]")
    }
    #endif
}

let WRITE_FILE_NAME_UUID = "uuid"
let WRITE_FILE_NAME_USER_NO = "user_no"

//let ENVIRONMENT_MODE=EnvironmentModeEnum.production
let ENVIRONMENT_MODE = EnvironmentModeEnum.staging // <-ステージングです。こちらでテストして下さい。

let SERVICE_ID = "サービスIDを入れてください"
let MERCHANT_ID = "店舗IDを入れてください"

let DNS_PRODUCTION = "https://sandbox.unid.net"
let DNS_STAGING = "https://stg-sandbox.unid.net"
let DNS_DEVELOP = "https://dev-sandbox.unid.net"

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let fileData = fileRead(fileName: WRITE_FILE_NAME_UUID)

        if (fileData.count == 0) {
            self.performSegue(withIdentifier: "userName", sender: self)
        }
    }

    @IBAction func testButton(_ sender: Any) {

        callCardRegister()
    }

    @IBAction func testButton2(_ sender: Any) {

        callPayment()
    }


    // Segue 準備

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "History") {
            let uuid = fileRead(fileName: WRITE_FILE_NAME_UUID)
            if (uuid.count > 0) {

                let historyViewController = segue.destination as? HistoryViewController
                historyViewController?.uuid = uuid

            }
        }


    }

    func fileSave(fileName: String, data: String) -> Bool {
        guard let dirURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            fatalError("フォルダURL取得エラー")
        }
        let fileURL = dirURL.appendingPathComponent(fileName + ".txt")
        do {
            try data.write(to: fileURL, atomically: true, encoding: .utf8)
        } catch {
            print("failed to write: \(error)")
            return false
        }
        return true
    }

    func fileRead(fileName: String) -> String {
        guard let dirURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return ""
//            fatalError("フォルダURL取得エラー")
        }
        let fileURL = dirURL.appendingPathComponent(fileName + ".txt")
        guard let fileContents = try? String(contentsOf: fileURL) else {
            return ""

            //            fatalError("ファイル読み込みエラー")
            
        }
        return fileContents
        
    }

    func getUuid(responseBrock: @escaping (String, Int, Int32, NSError?) -> ()) {
        let uuid = fileRead(fileName: WRITE_FILE_NAME_UUID)
        let userNoStr = fileRead(fileName: WRITE_FILE_NAME_USER_NO)

        if (uuid.count == 0) {
            performSegue(withIdentifier: "userName", sender: self)
        } else {
            guard var userNo = Int(userNoStr) else { return  }
            
            responseBrock(uuid, userNo, 0, nil)
        }
    }

    func callCardRegister() {
        getUuid { uuid, userNo, status, error in
            if (status != 0) {
                let alertController: UIAlertController =
                        UIAlertController(title: "エラー",
                                message: "サービスID、店舗IDまたは、uuIDがセットされていません",
                                preferredStyle: .alert)

                // Default のaction
                let defaultAction: UIAlertAction =
                        UIAlertAction(title: "はい",
                                style: .default,
                                handler: {
                                    (action: UIAlertAction!) -> Void in
                                    // 処理
                                })

                // actionを追加
                alertController.addAction(defaultAction)

                // UIAlertControllerの起動
                self.present(alertController, animated: true, completion: nil)

            } else {
                if (uuid == nil) {
                    let alertController: UIAlertController =
                            UIAlertController(title: "エラー",
                                    message: "サービスID、店舗IDまたは、uuIDがセットされていません",
                                    preferredStyle: .alert)

                    // Default のaction
                    let defaultAction: UIAlertAction =
                            UIAlertAction(title: "はい",
                                    style: .default,
                                    handler: {
                                        (action: UIAlertAction!) -> Void in
                                        // 処理
                                    })

                    // actionを追加
                    alertController.addAction(defaultAction)

                    // UIAlertControllerの起動
                    self.present(alertController, animated: true, completion: nil)

                    return
                }

                RoutePaySwift.callCardRegister(
                        uuid: uuid,
                        userNo: Int32(userNo),
                        viewController: self,
                        environmentMode: ENVIRONMENT_MODE,
                        callSuccess: { uuid, userNo in
                            DLog("保存成功")
                        },
                        callFailed: { int32, error in
                            DLog("Error[%@]", file: error.debugDescription)
                        })

            }
        }
    }

    func callPayment() {
        getUuid { uuid, userNo, status, error in
            if (status != 0) {
                let alertController: UIAlertController =
                        UIAlertController(title: "エラー",
                                message: "サービスID、店舗IDまたは、uuIDがセットされていません",
                                preferredStyle: .alert)

                // Default のaction
                let defaultAction: UIAlertAction =
                        UIAlertAction(title: "はい",
                                style: .default,
                                handler: {
                                    (action: UIAlertAction!) -> Void in
                                    // 処理
                                })

                // actionを追加
                alertController.addAction(defaultAction)

                // UIAlertControllerの起動
                self.present(alertController, animated: true, completion: nil)
            } else {
                if (uuid == nil) {
                    let alertController: UIAlertController =
                            UIAlertController(title: "エラー",
                                    message: "サービスID、店舗IDまたは、uuIDがセットされていません",
                                    preferredStyle: .alert)

                    // Default のaction
                    let defaultAction: UIAlertAction =
                            UIAlertAction(title: "はい",
                                    style: .default,
                                    handler: {
                                        (action: UIAlertAction!) -> Void in
                                        // 処理
                                    })

                    // actionを追加
                    alertController.addAction(defaultAction)

                    // UIAlertControllerの起動
                    self.present(alertController, animated: true, completion: nil)

                    return
                }

                RoutePaySwift.callPayment(
                        uuid: uuid,
                        userNo: Int32(userNo),
                        viewController: self,
                        environmentMode: ENVIRONMENT_MODE,
                        callSuccess: { s, int32 in
                            DLog("成功")

                        },
                        callFailed: { int32, error in
                            DLog("Error[%@]", file: error.debugDescription)

                        })

            }
        }
    }

}

