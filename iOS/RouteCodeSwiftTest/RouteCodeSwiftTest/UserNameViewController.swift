//
//  UserNameViewController.swift
//  RouteCodeSwiftTest
//
//  Created by GoYamada on 2022/07/22.
//

import RouteCode
import UIKit

class UserNameViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!

    @IBAction func buttonAction(_ sender: Any) {

        RoutePaySwift.initialUuid(
                serviceId: SERVICE_ID,
                merchantId: MERCHANT_ID,
                environmentMode: ENVIRONMENT_MODE,
                callSuccess: { uuid, userNo in
                    if let name = self.textField.text {
                        let ret = self.fileSave(fileName: WRITE_FILE_NAME_UUID, data: uuid!)
                        if (ret) {
                            let ret = self.fileSave(fileName: WRITE_FILE_NAME_USER_NO, data: String(userNo))
                            if (ret) {
                                self.navigationController?.popViewController(animated: true)
                            } else {
                                DLog("保存失敗")
                                let err2 = NSError(domain: "error", code: 1, userInfo: ["error": "error"])
                            }
                        } else {
                            DLog("保存失敗")
                            let err2 = NSError(domain: "error", code: 1, userInfo: ["error": "error"])
                        }

                    }
                }, callFailed: { status, error in

        })

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        let dt: Date = Date()
        let formatter: DateFormatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyyMMddHHmmss"
        let name = "ユーザー" + formatter.string(from: dt)

        self.textField.text = name
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func postUserName(name: String, uuid: String) {
        let dns: String

        switch (ENVIRONMENT_MODE) {
        case EnvironmentModeEnum.production:
            dns = DNS_PRODUCTION
            break;
        case EnvironmentModeEnum.staging:
            dns = DNS_STAGING
            break;
        case EnvironmentModeEnum.develop:
            dns = DNS_DEVELOP
            break;
        }


        let urlString = dns + "/set-user-data.php"
        let url = URL(string: urlString)!  //URLを生成
        var request = URLRequest(url: url)               //Requestを生成

        request.httpMethod = "POST"

        let parameter = ["name": name, "uuid": uuid]

        let parameterString = buildQueryWithDictionary(params: parameter)

        request.httpBody = parameterString.data(using: .utf8)

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            //非同期で通信を行う
            guard let data = data else {
                return
            }
            do {
                let encodedString: String? = String(data: data, encoding: .utf8)
                let restoreData = Data(base64Encoded: encodedString!)
                let restoreString = String(data: restoreData!, encoding: .utf8)

                DispatchQueue.main.async { [self] in

                    // メインスレッドで実行 UIの処理など
//                    self.textView.text = restoreString ?? ""

                }
                print()
            } catch let error {
                print(error)
            }
        }
        task.resume()


    }

    func buildQueryWithDictionary(params: Dictionary<String, String>) -> String {
        var parts: [String] = []

        for elem in params {
            let key = elem.key
            let value = elem.value

            let param: String = encodeURIComponent(str: key) + "=" + encodeURIComponent(str: value)

            parts.append(param)

        }

        return parts.joined(separator: "&")
    }

    func encodeURIComponent(str: String) -> String {
        let encodeUrlString = str.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        return encodeUrlString
    }

    func fileSave(fileName: String, data: String) -> Bool {
        guard let dirURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            fatalError("フォルダURL取得エラー")
        }
        let fileURL = dirURL.appendingPathComponent(fileName + ".txt")
        do {
            try data.write(to: fileURL, atomically: true, encoding: .utf8)
        } catch {
            print("failed to write: \(error)")
            return false
        }
        return true
    }
}
