# Android

## 設定方法
Java、Kotlin共に同じ方法です。

- 「RouteCode-release.aar」が本体となります。

1. ライブラリの設定 (root)/app/libs フォルダに「RouteCode-release.aar」をおく

2. アプリケーション Gradle のビルド依存関係にライブラリを指定します。

```
dependencies {
implementation fileTree(dir: 'libs', include: ['*.jar','*.aar'])
...
```

3. アプリケーション Gradle のビルド依存関係に QR コード用のカメラ・バーコードライブラリを追加します。

```
dependencies {
...
implementation 'com.google.mlkit:barcode-scanning:17.0.2'
implementation 'androidx.camera:camera-camera2:1.2.0-alpha01'
implementation 'androidx.camera:camera-lifecycle:1.2.0-alpha01'
implementation 'androidx.camera:camera-view:1.2.0-alpha01'
...
```
