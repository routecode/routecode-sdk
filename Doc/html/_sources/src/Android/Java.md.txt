## Java

### 環境について

本SDKでは、本番環境と検証環境の2つのサイトが用意されています。
メソッドの引数に、「EnvironmentMode environmentMode」ある場合は、 サーバ環境を選択出来ます。 設定値は、以下の通りとなります。

| 環境  | url                           |
|-----|-------------------------------|
| 本番  | EnvironmentModeEnumProduction |
| 検証  | EnvironmentModeEnumStaging    |

### RoutePayクラス

| 名称    | 内容                              |
|-------|---------------------------------|
| パッケージ | com.platfield.unidsdk.routecode |
| クラス名  | RoutePay                        |

#### Declaration

クラスの初期化、クレジットカード登録および決済を行います。

#### メソッド

##### 初期化関数(環境モード付き)

RoutePayクラスのinitialUuidServiceIdを使用して初期化します。
サービスIDおよび店舗IDをもとにuuidおよびユーザー番号を取得します。
このuuidを元に決済します。

###### 関数宣言

```java
public class RoutePay {
    public void initialUuid(
            String serviceId,
            String merchantId,
            Activity activity,
            EnvironmentMode environmentMode,
            ResponseInitialUuidCallback callback
    );
}
```

- パラメータ

| 変数名             | 名称      |
|-----------------|---------|
| serviceId       | サービスID  |
| merchantId      | 店舗ID    |
| activity        | アクティビティ |
| environmentMode | サーバー環境  |
| callBack        | コールバック  |

###### コールバック関数

```java
public class RoutePay {
    public interface ResponseInitialUuidCallback {
        public void success(String uuid, int userNo);

        public void failed(int status, String message);
    }

}
```

- パラメータ
  (成功)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | uuid | uuid |
  | userNo | ユーザー番号 |

  (失敗)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | status | エラーコードが入ってきます。(現在のところ-1固定) |
  | error | エラー内容が入ってきます |

###### 例

```java
public class MainActivity extends AppCompatActivity {
    void test() {
        RoutePay routePay = new RoutePay();
        routePay.initialUuid(
                MainActivity.SERVICE_ID,
                MainActivity.MERCHANT_ID,
                this,
                MainActivity.ENVIRONMENT_MODE,
                new RoutePay.ResponseInitialUuidCallback() {

                    @Override
                    public void success(String uuidString, int userNo) {
                        // データ保存
                        UserData userData = new UserData(uuidString, userNo);
                        String userDataString = StringSerializer.serialize(userData);

                        fileSave(USER_DATA_FILE_NAME, userDataString);

                    }

                    @Override
                    public void failed(int i, String s) {
                        Toast.makeText(m_activity, "initialUuid:error2:" + s, Toast.LENGTH_LONG).show();

                    }
                });
    }
}
```

##### 初期化関数(環境モードなし)

「初期化関数(環境モード付き)」の環境モードが本番環境になっているバージョンです。

###### 関数宣言

```java
public class RoutePay {
    public void initialUuid(
            String serviceId,
            String merchantId,
            Activity activity,
            ResponseInitialUuidCallback callback
    );
}
```

- パラメータ

| 変数名        | 名称      |
|------------|---------|
| serviceId  | サービスID  |
| merchantId | 店舗ID    |
| activity   | アクティビティ |
| callBack   | コールバック  |

###### コールバック関数

```java
public class RoutePay {
    public interface ResponseInitialUuidCallback {
        public void success(String uuid, int userNo);

        public void failed(int status, String message);
    }
}
```

- パラメータ
  (成功)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | uuid | uuid |
  | userNo | ユーザー番号 |

  (失敗)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | status | エラーコードが入ってきます。(現在のところ-1固定) |
  | error | エラー内容が入ってきます |
  |

###### 例

「初期化(環境モード付き)」関数と同じですので省略します。

##### クレジットカード登録関数(環境モード付き)

クレジットカードを登録します。
このメソッドをコールすると、GUIが表示されます。
あらかじめ、初期化は行ってください。

###### 関数宣言

```java
public class RoutePay {
    public void callCardRegister(
            String uuid,
            int userNo,
            Activity activity,
            EnvironmentMode environmentMode,
            ResponseCardRegistCallback callback
    );
}
```

- パラメータ

| 変数名             | 名称      |
|-----------------|---------|
| uuid           | ユーザーID           | 
| userNo         | ユーザー番号           |
| activity        | アクティビティ |
| environmentMode | サーバー環境  | 
| callBack        | コールバック  |

###### コールバック関数

```java
public class RoutePay {
    public interface ResponseCardRegistCallback {
        public void success(String uuid, int userNo);

        public void failed(int status, String message);
    }
}
```

- パラメータ
  (成功)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | uuid | uuid |
  | userNo | ユーザー番号 |

  (失敗)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | status | エラーコードが入ってきます。(現在のところ-1固定) |
  | error | エラー内容が入ってきます |

###### 例

```Java
public class MainActivity extends AppCompatActivity {
    void test() {
        RoutePay routePay = new RoutePay();
        routePay.callCardRegister(
                userData.getUuid(),
                userData.getUserNo(),
                m_activity,
                ENVIRONMENT_MODE,
                new RoutePay.ResponseCardRegistCallback() {
                    @Override
                    public void success(String s, int i) {
                        Toast.makeText(getApplicationContext(), "カード登録完了", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failed(int i, String s) {
                        Toast.makeText(getApplicationContext(), "ユーザー情報取得失敗", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
```

##### クレジットカード登録関数(環境モードなし)

###### 関数名

```java
public class RoutePay {
    public void callCardRegister(
            String uuid,
            int userNo,
            Activity activity,
            ResponseCardRegistCallback callback
    );
}
```

- パラメータ

| 変数名      | 名称            |
|----------|---------------|
| uuid     | ユーザーID        | 
| userNo   | ユーザー番号        |
| activity | ユーザー情報生成レスポンス |
| callBack | コールバック        |

###### コールバック関数

```java
public class RoutePay {
    public interface ResponseCardRegistCallback {
        public void success(String uuid, int userNo);

        public void failed(int status, String message);
    }
}
```

- パラメータ
  (成功)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | uuid | uuid |
  | userNo | ユーザー番号 |

  (失敗)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | status | エラーコードが入ってきます。(現在のところ-1固定) |
  | error | エラー内容が入ってきます |
  |

###### 例

環境モード付き用の関数と内容は同じですので省略


##### 受付または決済関数(環境モード付き)

受付もしくわ決済処理認証を行うAPIです。
APIをコールすると、GUIが表示されます。
あらかじめ、ユーザー情報をサービスID、店舗ID、uuid、クライアント用秘密鍵、サーバ用公開鍵がセットされているユーザー情報を引数に設定してください。

```java
public class RoutePay {
    public void callPayment(
            String uuid,
            int userNo,
            Activity activity,
            EnvironmentMode environmentMode,
            ResponsePaymentCallback callback
    );
}
```

- パラメータ

| 変数名             | 名称            |
|-----------------|---------------|
| uuid            | ユーザーID        | 
| userNo          | ユーザー番号        |
| environmentMode | サーバー環境        | 
| activity        | ユーザー情報生成レスポンス |
| callBack        | コールバック        |

###### コールバック関数

```java
public class RoutePay {
    public interface ResponsePaymentCallback {
        public void success(String uuid, int userNo);

        public void failed(int status, String message);
    }

}
```

- パラメータ
  (成功)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | uuid | uuid |
  | userNo | ユーザー番号 |

  (失敗)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | status | エラーコードが入ってきます。(現在のところ-1固定) |
  | error | エラー内容が入ってきます |
  |

###### 例

```Java
public class MainActivity extends AppCompatActivity {
    void test() {

        routePay.callPayment(
                userData.getUuid(),
                userData.getUserNo(),
                m_activity,
                ENVIRONMENT_MODE,
                new RoutePay.ResponsePaymentCallback() {
                    @Override
                    public void success(String s, int i) {
                        Toast.makeText(getApplicationContext(), "決済完了", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failed(int i, String s) {
                        Toast.makeText(getApplicationContext(), "失敗", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}
```

##### 受付または決済関数(環境モードなし)

「受付または決済関数(環境モード付き)」の環境モードが本番環境になっているバージョンです。

###### 関数宣言

```java
public class RoutePay {
    public void callPayment(
            String uuid,
            int userNo,
            Activity activity,
            ResponsePaymentCallback callback
    );
}
```

-
- パラメータ

| 変数名      | 名称            |
|----------|---------------|
| userInfo | ユーザー情報        | 
| activity | ユーザー情報生成レスポンス |
| callBack | コールバック        |

###### コールバック関数

```java
public class RoutePay {
    public interface ResponsePaymentCallback {
        public void success(String uuid, int userNo);

        public void failed(int status, String message);
    }

}
```

- パラメータ
  (成功)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | uuid | uuid |
  | userNo | ユーザー番号 |

  (失敗)
  | 変数名 | 名称 |
  |-----------------|---------------|
  | status | エラーコードが入ってきます。(現在のところ-1固定) |
  | error | エラー内容が入ってきます |
  |

###### 例

「受付または決済関数(環境モードあり)」関数と同じですので省略します。

