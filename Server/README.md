# RouteCode for PHP

## サンプルについて

サンプルサイトとして、以下を用意しております。

### 検証環境

    https://stg-sandbox.unid.net/

## サンプルの内容

    

## フォルダ構成



```
.
├── README.md
└── Sample
    ├── assets 								       // アセットコンテンツ
    ├── const.php								   // 定数
    ├── processing.php                              // 各RoutePayAPI処理メソッド群
    ├── index.php								   // トップページ
    ├── common.js                                   // 共通JSモジュール
    ├── access-token.php                            // アクセストークン発行画面
    ├── get-access-token.php                        // アクセストークン取得処理 
    ├── change-password-processing.php              // パスワード変更処理
    ├── change-password.php                         // パスワード変更画面    
    ├── get_receipt_data.php                        // サーバー内レシート情報取得
    ├── select.php                                  // 受付・決済選択画面
    ├── pay-refund-list.php                         // 決済および返金リスト画面
    ├── reception-input.php                         // 受付画面
    ├── reception-processing-pay.php                // 受付用決済処理
    ├── reception-list.php                          // 受付番号リスト画面
    ├── reception-processing-delete.php             // 受付番号削除処理
    ├── callback.php                                // 受付･決済後のコールバック画面
    ├── pay-refund-input.php                        // 返金画面
    ├── pay-processing-refund.php                   // 返金処置
    ├── user-list.php                                // ユーザーリスト
    ├── user-data.php                                // ユーザー毎レシート履歴表示画面 
    └── user                                         // 各ユーザーレシート情報取得フォルダ

```



