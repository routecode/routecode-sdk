function ClearCallbackButtonClick() {
    const target = document.getElementById("callback_url");
    target.value = "";

}

function ClearAccessTokenButtonClick() {
    const target = document.getElementById("access_token");
    target.value = "";

}

function formPost(path, params) {

    const form = document.createElement('form');
    form.method = 'post';
    form.action = path;

    for (const key in params) {
        if (params.hasOwnProperty(key)) {
            const hiddenField = document.createElement('input');
            hiddenField.type = 'hidden';
            hiddenField.name = key;
            hiddenField.value = params[key];

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

function deleteAccessToken(path, accessToken) {
    const xhr = new XMLHttpRequest();
    xhr.open('post', path);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

    const data = {
        'access_token': accessToken,
    };

    // JSON文字列に変換
    const jsonText = JSON.stringify(data);

    xhr.send(jsonText);

    xhr.onload = function () {
        let responseObj = xhr.response;
        console.log(responseObj);
    };
}

function paySend(url, payMode) {
    // form を動的に生成
    const form = document.createElement('form');
    form.action = url;
    form.method = 'post';
    // body に追加
    document.body.append(form);
    // formdta イベントに関数を登録(submit する直前に発火)
    form.addEventListener('formdata', (e) => {
        const fd = e.formData;

        const accessTokenElement = document.getElementById("access_token");

        fd.set('access_token', accessTokenElement.value);

        if (payMode === 0) {
            // 金額
            const amountElement = document.getElementById("amount");
            fd.set('pay_mode','0');
            fd.set('amount', amountElement.value);

        } else {
            // 金額
            const maxAmountElement = document.getElementById("max_amount");
            fd.set('pay_mode','1');
            fd.set('max_amount', maxAmountElement.value);
        }
    });

    // submit
    form.submit();
}

function payRefund(path, receiptCode) {
    const xhr = new XMLHttpRequest();
    xhr.open('post', path);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');

    const data = {"receipt_code": receiptCode};

    // JSON文字列に変換
    const jsonText = JSON.stringify(data);
    xhr.send(jsonText);

    xhr.onload = function () {
        let responseObj = xhr.response;
        const responseJson = JSON.parse(responseObj);
        insertPayRefundData(responseJson);
    }
}

function payListReload() {
    // form を動的に生成
    const form = document.createElement('form');
    form.action = 'pay-refund-list.php';
    form.method = 'post';
    // body に追加
    document.body.append(form);
    // formdta イベントに関数を登録(submit する直前に発火)
    form.addEventListener('formdata', (e) => {
        const fd = e.formData;

        const startPayDateTimeCheckboxElement = document.getElementById("start_pay_date_time_checkbox");
        const startPayDateTimeElement = document.getElementById("start_pay_date_time");

        const endPayDateTimeCheckboxElement = document.getElementById("end_pay_date_time_checkbox");
        const endPayDateTimeElement = document.getElementById("end_pay_date_time");

        const startRefundDateTimeCheckboxElement = document.getElementById("start_refund_date_time_checkbox");
        const startRefundDateTimeElement = document.getElementById("start_refund_date_time");

        const endRefundDateTimeCheckboxElement = document.getElementById("end_refund_date_time_checkbox");
        const endRefundDateTimeElement = document.getElementById("end_refund_date_time");


        const viewModeElement = document.getElementById("view_mode");

        let viewMode = '0';

        for ( let i=0,l=viewModeElement.length; l>i; i++ ) {
            if ( viewModeElement[i].selected ) {
                viewMode = i.toString();
            }
        }

        fd.set('start_pay_date_time_checkbox',startPayDateTimeCheckboxElement.checked  === true ? "1" : "0");
        fd.set('start_pay_date_time', startPayDateTimeElement.value);

        fd.set('end_pay_date_time_checkbox',endPayDateTimeCheckboxElement.checked  === true ? "1" : "0");
        fd.set('end_pay_date_time', endPayDateTimeElement.value);

        fd.set('start_refund_date_time_checkbox',startRefundDateTimeCheckboxElement.checked  === true ? "1" : "0");
        fd.set('start_refund_date_time', startRefundDateTimeElement.value);

        fd.set('end_refund_date_time_checkbox',endRefundDateTimeCheckboxElement.checked  === true ? "1" : "0");
        fd.set('end_refund_date_time', endRefundDateTimeElement.value);


        fd.set('view_mode', viewMode);

    });

    // submit
    form.submit();
}

function receptionProcessingDelete(receiptProcessingCode ){
    // form を動的に生成
    const form = document.createElement('form');
    form.action = 'reception-processing-delete.php';
    form.method = 'post';
    // body に追加
    document.body.append(form);
    // formdta イベントに関数を登録(submit する直前に発火)
    form.addEventListener('formdata', (e) => {
        const fd = e.formData;

        fd.set("receipt_processing_code", receiptProcessingCode);
    });

    // submit
    form.submit();
}

function receptionProcessingPay(receiptProcessingCode ){
    // form を動的に生成
    const form = document.createElement('form');
    form.action = 'reception-processing-pay.php';
    form.method = 'post';
    // body に追加
    document.body.append(form);
    // formdta イベントに関数を登録(submit する直前に発火)
    form.addEventListener('formdata', (e) => {
        const fd = e.formData;

        const amountElement = document.getElementById("amount");

        fd.set("receipt_processing_code", receiptProcessingCode);
        fd.set("amount", amountElement.value);
    });

    // submit
    form.submit();
}

function payProcessingRefund(){
    // form を動的に生成
    const form = document.createElement('form');
    form.action = 'pay-processing-refund.php';
    form.method = 'post';
    // body に追加
    document.body.append(form);
    // formdta イベントに関数を登録(submit する直前に発火)
    form.addEventListener('formdata', (e) => {
        const fd = e.formData;

        const receiptCodeElement = document.getElementById("receipt_code");
        const amountElement = document.getElementById("amount");

        fd.set("receipt_code",  receiptCodeElement.value);
        // fd.set("amount", amountElement.value);
    });

    // submit
    form.submit();
}

function payRefundInput(params){
    // form を動的に生成
    const form = document.createElement('form');
    form.action = 'pay-refund-input.php';
    form.method = 'post';
    // body に追加
    document.body.append(form);
    // formdta イベントに関数を登録(submit する直前に発火)
    form.addEventListener('formdata', (e) => {
        const fd = e.formData;
        fd.set("params", JSON.stringify(params));
    });

    // submit
    form.submit();
}


function receptionInput(params){
    // form を動的に生成
    const form = document.createElement('form');
    form.action = 'reception-input.php';
    form.method = 'post';
    // body に追加
    document.body.append(form);
    // formdta イベントに関数を登録(submit する直前に発火)
    form.addEventListener('formdata', (e) => {
        const fd = e.formData;
        fd.set("params", JSON.stringify(params));
    });

    // submit
    form.submit();
}

function receptionListReload() {
    // form を動的に生成
    const form = document.createElement('form');
    form.action = 'reception-list.php';
    form.method = 'post';
    // body に追加
    document.body.append(form);
    // formdta イベントに関数を登録(submit する直前に発火)
    form.addEventListener('formdata', (e) => {
        const fd = e.formData;

        const startDateTimeCheckboxElement = document.getElementById("start_date_time_checkbox");
        const startDateTimeElement = document.getElementById("start_date_time");

        const endDateTimeCheckboxElement = document.getElementById("end_date_time_checkbox");
        const endDateTimeElement = document.getElementById("end_date_time");

        fd.set('start_date_time_checkbox',startDateTimeCheckboxElement.checked  == true ? "1" : "0");
        fd.set('start_date_time', startDateTimeElement.value);

        fd.set('end_date_time_checkbox',endDateTimeCheckboxElement.checked  == true ? "1" : "0");
        fd.set('end_date_time', endDateTimeElement.value);

    });

    // submit
    form.submit();
}


function insertPayRefundData(responseJson) {
    const payRefundButton = document.getElementById("pay-refund-button")
    payRefundButton.remove();

    const container = document.getElementById("pay-refund")

    const hr = document.createElement("hr");

    container.appendChild(hr);

    const br = document.createElement("br");

    container.appendChild(br);
    container.appendChild(br);
    container.appendChild(br);

    const p = document.createElement("p");
    p.appendChild(document.createTextNode("返品レシート"));
    container.appendChild(p);

    const div = document.createElement("div");
    div.className = "table-responsive";

    const tbl = document.createElement("table");

    tbl.className = 'table table-striped table-hover';

    const tblThead = document.createElement("thead");

    const rowThead = document.createElement("tr");
    const cellThead1 = document.createElement("th");
    cellThead1.setAttribute('scope', 'col');
    const cellThead1Text = document.createTextNode("キー");
    cellThead1.appendChild(cellThead1Text);
    rowThead.appendChild(cellThead1);

    const cellThead2 = document.createElement("th");
    cellThead2.setAttribute('scope', 'col');
    const cellThead2Text = document.createTextNode("内容");
    cellThead2.appendChild(cellThead2Text);
    rowThead.appendChild(cellThead2);
    tblThead.appendChild(rowThead);

    const tblBody = document.createElement("tbody");

    for (const item in responseJson) {

        const row = document.createElement("tr");
        const cell = document.createElement("th");
        cell.setAttribute('scope', 'col');
        const cellText = document.createTextNode(item);
        cell.appendChild(cellText);
        row.appendChild(cell);

        const cell2 = document.createElement("td");
        let textData = responseJson[item];

        if (typeof textData === 'string') {
            const textDataArray = textData.split(/\r\n|\n/);
            for (let i = 0; i < textDataArray.length; i++) {
                const cell2Text = document.createTextNode(textDataArray[i]);
                cell2.appendChild(cell2Text);
                const br2 = document.createElement("br");
                cell2.appendChild(br2);
            }
        } else {
            const cell2Text = document.createTextNode(textData);
            cell2.appendChild(cell2Text);
        }

        row.appendChild(cell2);
        tblBody.appendChild(row);
    }

    tbl.appendChild(tblThead);
    tbl.appendChild(tblBody);
    div.appendChild(tbl);

    container.appendChild(div);

    const elm = document.documentElement;
    const bottom = elm.scrollHeight - elm.clientHeight;

    window.scroll(0, bottom);

}

function userData(uuid ){
    // form を動的に生成
    const form = document.createElement('form');
    form.action = 'user-data.php';
    form.method = 'post';
    // body に追加
    document.body.append(form);
    // formdta イベントに関数を登録(submit する直前に発火)
    form.addEventListener('formdata', (e) => {
        const fd = e.formData;

        fd.set("uuid", uuid);
    });

    // submit
    form.submit();
}
