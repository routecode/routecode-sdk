<?php
require_once "const.php";

function get_access_token($serviceId,
                          $merchantId,
                          $registerId,
                          $password,
                          $callbackUrl)
{
// get cURL resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, URL_ACCESS_TOKEN);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $header = [
        'x-service-id: ' . $serviceId,
        'x-merchant-id: ' . $merchantId,
        'x-register-id: ' . $registerId,
        'x-password: ' . $password,
    ];


    if (!empty($callbackUrl)) {
        $header[] = 'x-callback-url:' . $callbackUrl;
    }

    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    $response = curl_exec($ch);

    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

    if ($httpCode == 200) {
        $responseArray = json_decode($response, true);
    } else {
        $accessToken = "----------------------";

        $responseArray = null;
    }

    curl_close($ch);

    return $responseArray;
}

function pay_processing_refund(
    $accessToken,
    $receiptCode
)
{
// get cURL resource
    $ch = curl_init();

// set url
    curl_setopt($ch, CURLOPT_URL, URL_PAY_REFUND);

// set method
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

// return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


    $header = [
        'X-Access-Token: ' . $accessToken,
        'Content-Type: application/json; charset=utf-8',
    ];

// set headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);


// form body
    $json_array = [
        'receipt_code' => $receiptCode,
    ];

    $body = json_encode($json_array);

// set body
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

// send the request and save response to $response
    $response = curl_exec($ch);

// stop if fails
    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

    if ($httpCode == 200) {
        $responseArray = json_decode($response, true);

//        echo "header:" . nl2br(print_r($header,true));
//        echo "json_array:" . nl2br(print_r($json_array,true));
//        echo "responseArray:" . nl2br(print_r($responseArray,true));

    } else {
        $responseArray = null;
    }

    curl_close($ch);

    return $responseArray;
}

function pay_refund_list(
    $accessToken,
    $startPayDateTimeCheckbox,
    $startPayUnixTime,
    $endPayDateTimeCheckbox,
    $endPayUnixTime,
    $startRefundDateTimeCheckbox,
    $startRefundUnixTime,
    $endRefundDateTimeCheckbox,
    $endRefundUnixTime,
    $viewMode
)
{

// get cURL resource
    $ch = curl_init();

// set url
    curl_setopt($ch, CURLOPT_URL, URL_PAY_LIST);

// set method
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

// return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $header = [
        'X-Access-Token: ' . $accessToken,
        'Content-Type: application/json; charset=utf-8',
    ];

    // set headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    $json_array ['view_mode'] = $viewMode;

    if ($startPayDateTimeCheckbox == 1) {
        $json_array ['start_pay_time'] = $startPayUnixTime;

    }

    if ($endPayDateTimeCheckbox == 1) {
        $json_array ['end_pay_time'] = $endPayUnixTime;
    }

    if ($startRefundDateTimeCheckbox == 1) {
        $json_array ['start_Refund_time'] = $startRefundUnixTime;

    }

    if ($endRefundDateTimeCheckbox == 1) {
        $json_array ['end_refund_time'] = $endRefundUnixTime;
    }

    $body = json_encode($json_array);

// set body
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

// send the request and save response to $response
    $response = curl_exec($ch);

// stop if fails
    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

    if ($httpCode == 200) {
        $responseArray = json_decode($response, true);
//        echo "header:" . nl2br(print_r($header,true));
//        echo "json_array:" . nl2br(print_r($json_array,true));
//        echo "responseArray:" . nl2br(print_r($responseArray,true));

    } else {
        $responseArray = null;
    }

    curl_close($ch);

    return $responseArray;
}

function reception_list(
    $accessToken,
    $startDateTimeCheckbox,
    $startUnixTime,
    $endDateTimeCheckbox,
    $endUnixTime

)
{
    // get cURL resource
    $ch = curl_init();

// set url
    curl_setopt($ch, CURLOPT_URL, URL_PAY_RECEPTION_LIST);

// set method
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

// return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


    $header = [
        'X-Access-Token: ' . $accessToken,
        'Content-Type: application/json; charset=utf-8',
    ];

// set headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

// json body

    if ($startDateTimeCheckbox == 1) {
        $json_array ['start_time'] = $startUnixTime;

    }

    if ($endDateTimeCheckbox == 1) {
        $json_array ['end_time'] = $endUnixTime;
    }

    $body = json_encode($json_array);

// set body
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

// send the request and save response to $response
    $response = curl_exec($ch);

// stop if fails
    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

    if ($httpCode == 200) {
        $responseArray = json_decode($response, true);

//        echo "header:" . nl2br(print_r($header,true));
//        echo "json_array:" . nl2br(print_r($json_array,true));
//        echo "responseArray:" . nl2br(print_r($responseArray,true));
    } else {
        $responseArray = null;
    }
// close curl resource to free up system resources
    curl_close($ch);

    return $responseArray;
}

function reception_processing_delete(
    $accessToken,
    $receipt_processing_code
)
{
    // get cURL resource
    $ch = curl_init();

// set url
    curl_setopt($ch, CURLOPT_URL, URL_PAY_DELETE_RECEPTION);

// set method
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

// return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $header = [
        'X-Access-Token: ' . $accessToken,
        'Content-Type: application/json; charset=utf-8',
    ];

// set headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

// json body
    $json_array = [
        'receipt_processing_code' => $receipt_processing_code
    ];
    $body = json_encode($json_array);

// set body
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

// send the request and save response to $response
    $response = curl_exec($ch);

// stop if fails
    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

    if ($httpCode == 200) {
        $responseArray = json_decode($response, true);

//        echo "header:" . nl2br(print_r($header,true));
//        echo "json_array:" . nl2br(print_r($json_array,true));
//        echo "responseArray:" . nl2br(print_r($responseArray,true));

    } else {
        $responseArray = null;
    }

// close curl resource to free up system resources
    curl_close($ch);

    return $responseArray;
}

function reception_processing_pay(
    $accessToken,
    $receipt_processing_code,
    $amount

)
{

    // get cURL resource
    $ch = curl_init();

// set url
    curl_setopt($ch, CURLOPT_URL, URL_PAY_RECEPTION_SETTLEMENT);

// set method
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

// return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $header = [
        'X-Access-Token: ' . $accessToken,
        'Content-Type: application/json; charset=utf-8',
    ];

// set headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

// json body
    $json_array = [
        'receipt_processing_code' => $receipt_processing_code,
        'price_data' => $amount
    ];
    $body = json_encode($json_array);

// set body
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

// send the request and save response to $response
    $response = curl_exec($ch);

// stop if fails
    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

    if ($httpCode == 200) {
        $responseArray = json_decode($response, true);

//        echo "header:" . nl2br(print_r($header,true));
//        echo "json_array:" . nl2br(print_r($json_array,true));
//        echo "responseArray:" . nl2br(print_r($responseArray,true));

    } else {
        $responseArray = null;
    }
    curl_close($ch);

    return $responseArray;
}


function changePassword(
    $accessToken,
    $oldPassword,
    $repeatPassword
)
{
// get cURL resource
    $ch = curl_init();

// set url
    curl_setopt($ch, CURLOPT_URL, URL_CHANGE_PASSWORD);

// set method
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

// return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $header = [
        'X-Access-Token: ' . $accessToken,
        'Content-Type: application/json; charset=utf-8',
    ];


// set headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

// json body
    $json_array = [
        'old_password' => $oldPassword,
        'new_password' => $repeatPassword,
    ];
    $body = json_encode($json_array);

// set body
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

// send the request and save response to $response
    $response = curl_exec($ch);

// stop if fails
    if (!$response) {
        die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
    }

    $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

    if ($httpCode == 200) {
        $responseArray = json_decode($response, true);

    } else {
        $responseArray = null;
    }

// close curl resource to free up system resources
    curl_close($ch);

    return $responseArray;
}

function saveAccessToken($accessToken)
{
//     $accessTokenPath = "AccessToken";
//     if (!file_exists($accessTokenPath)) {
//         // 文字列をaccess_token.txtに書き込む
//         if (!mkdir($accessTokenPath, 0777, true)) {
//             die('Failed to create directories...');
//         }
//     }
// 
//     file_put_contents("AccessToken/access_token.txt", $accessToken);

    setcookie("AccessToken", $accessToken);


}

function getAccessToken()
{
    // $accessToken = file_get_contents("AccessToken/access_token.txt");
    // if (!$accessToken) {
    //     header('Location: ' . URL_MY_ACCESS_TOKEN);
    //     exit();
    // }

    $accessToken = $_COOKIE["AccessToken"];
    return $accessToken;
}

function saveUserData($uuid, $receipt_data)
{
    $userPath = "user";
    if (!file_exists($userPath)) {
        // 文字列をaccess_token.txtに書き込む
        if (!mkdir($userPath, 0777, true)) {
            die('Failed to create directories...');
        }
    }

    $filename = "user/${uuid}.txt";
// ファイルを開く（'w'は書き込みモード）
    $fp = fopen($filename, 'a');
// ファイルに書き込む
    $data = $receipt_data . "\n";
    fputs($fp, $data);
// ファイルを閉じる
    fclose($fp);
}
