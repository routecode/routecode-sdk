<?php

if(empty($_GET['uuid']) ) {
    die('Failed to create directories...');
}

$uuid = $_GET['uuid'];

$uuid_receipt_data = file_get_contents("user/" . $uuid . ".txt");

if ($uuid_receipt_data === false) {
    header('Location: ' . URL_MY_ACCESS_TOKEN );
    exit();
}

echo base64_encode($uuid_receipt_data);
