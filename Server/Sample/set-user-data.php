<?php
require_once "processing.php";

if(empty($_POST['uuid']) ) {
    die('Failed to create directories...');
}

$uuid = $_POST['uuid'];
$name = $_POST['name'];

saveUserData($uuid, $name);

return "ok";
