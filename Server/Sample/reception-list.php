<?php
require_once "processing.php";

$accessToken = getAccessToken();

$objDateTime = new DateTime();
if (isset($_POST['start_date_time_checkbox'])) {
    if (!empty($_POST['start_date_time_checkbox'])) {
        $startDateTimeCheckbox = 1;
    } else {
        $startDateTimeCheckbox = 0;
    }
} else {
    $startDateTimeCheckbox = 1;
}
if (!empty($_POST['start_date_time'])) {
    $startDateTimeStr = $_POST['start_date_time'];
    $startDateTimeArray = explode("T", $startDateTimeStr);
    $startDate = $startDateTimeArray[0];
    $startTime = $startDateTimeArray[1];
} else {
    $startDate = $objDateTime->format('Y-m-d');
    $startTime = "00:00";
}
$startUnixTime = strtotime($startDate . " " . $startTime);

if (isset($_POST['end_date_time_checkbox'])) {
    if (!empty($_POST['end_date_time_checkbox'])) {
        $endDateTimeCheckbox = 1;
    } else {
        $endDateTimeCheckbox = 0;
    }

} else {
    $endDateTimeCheckbox = 1;
}
if (!empty($_POST['end_date_time'])) {
    $endDateTimeStr = $_POST['end_date_time'];
    $endDateTimeArray = explode("T", $endDateTimeStr);
    $endDate = $endDateTimeArray[0];
    $endTime = $endDateTimeArray[1];
} else {
    $endDate = $objDateTime->format('Y-m-d');
    $endTime = "23:59";
}
$endUnixTime = strtotime($endDate . " " . $endTime);

$responseArray = reception_list(
    $accessToken,
    $startDateTimeCheckbox,
    $startUnixTime,
    $endDateTimeCheckbox,
    $endUnixTime
);

if(is_null($responseArray)) {
    die( 'Error: not http');
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title> <?php echo LABEL_RECEPTION_LIST . LABEL_HYPHEN . PRODUCT_NAME . LABEL_ENVIRONMENT; ?></title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Noto+Sans+Inscriptional+Pahlavi&amp;display=swap">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css">
    <script type="text/javascript" src="common.js"></script>
</head>

<body id="page-top">
<div id="wrapper">
    <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0">
        <div class="container-fluid d-flex flex-column p-0"><a
                    class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0"
                    href="index.php">
                <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-solar-panel"></i></div>
                <div class="sidebar-brand-text mx-3"><span>Route Pay</span></div>
            </a>
            <hr class="sidebar-divider my-0">
            <ul class="navbar-nav text-light" id="accordionSidebar">
                <li class="nav-item"></li>
                <li class="nav-item">
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_ACCESS_TOKEN) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_ACCESS_TOKEN; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"
                             stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                             stroke-linejoin="round" class="icon icon-tabler icon-tabler-login">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2"></path>
                            <path d="M20 12h-13l3 -3m0 6l-3 -3"></path>
                        </svg>
                        <span><?php echo LABEL_ACCESS_TOKEN; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_ACCESS_SELECT) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_ACCESS_SELECT; ?>"><i class="far fa-list-alt"></i><span><?php echo LABEL_SELECT; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_RECEPTION_LIST) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_RECEPTION_LIST; ?>"><i class="far fa-list-alt"></i><span><?php echo LABEL_RECEPTION_LIST; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_PAY_REFUND_LIST) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_PAY_REFUND_LIST; ?>"><i class="far fa-list-alt"></i><span><?php echo LABEL_PAY_REFUND_LIST; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_USER_LIST) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_USER_LIST; ?>"><i class="far fa-list-alt"></i><span><?php echo LABEL_USER_LIST; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_CHANGE_PASSWORD) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_CHANGE_PASSWORD; ?>" ><i class="far fa-list-alt"></i><span><?php echo LABEL_USER_CHANGE_PASSWORD; ?></span></a>

                </li>
            </ul>
            <div class="text-center d-none d-md-inline">
                <button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button>
            </div>
        </div>
    </nav>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                <div class="container-fluid">
                    <button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i
                                class="fas fa-bars"></i></button>
                    <h1>
                        <span style="color: rgba(var(--bs-dark-rgb), var(--bs-text-opacity)) ; background-color: rgb(248, 249, 252);"><?php echo LABEL_ENVIRONMENT; ?></span><br>
                    </h1>
                </div>
            </nav>
            <div class="container-fluid">
                <h3 class="text-dark mb-1"><span
                            style="color: rgba(var(--bs-dark-rgb), var(--bs-text-opacity)) ;"><?php echo LABEL_RECEPTION_LIST; ?></span></h3>
                <div class="card shadow">
                    <div class="card-body">
                        <form>
                            <div class="input-group"></div>
                        </form>
                        <div class="row">
                            <div class="col col-4">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input name="start_date_time_checkbox" id="start_date_time_checkbox"
                                               type="checkbox" <?php echo $startDateTimeCheckbox == 1 ? "checked" : "" ?> >
                                        <label class="form-label" for="start_date_time_checkbox">開始&nbsp;</label>
                                        <input name="start_date_time" id="start_date_time" type="datetime-local"
                                               value="<?php echo $startDate . "T" . $startTime ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col col-4">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input name="end_date_time_checkbox" id="end_date_time_checkbox"
                                               type="checkbox" <?php echo $endDateTimeCheckbox == 1 ? "checked" : "" ?> >
                                        <label class="form-label" for="end_date_time_checkbox">終了&nbsp;</label>
                                        <input name="end_date_time" id="end_date_time" type="datetime-local"
                                               value="<?php echo $endDate . "T" . $endTime ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col col-4">
                                <div class="text-end">
                                    <button class="btn btn-primary" type="button"
                                            onclick="receptionListReload(); return false;">検索
                                    </button>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="table-responsive table mt-2" id="dataTable-1" role="grid"
                             aria-describedby="dataTable_info">
                            <table class="table my-0" id="dataTable">
                                <thead>
                                <tr>
                                    <th>受付番号</th>
                                    <th>uuid</th>
                                    <th>上限金額</th>
                                    <th>受付期限</th>
                                    <th>レジ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($responseArray as &$value) {
                                    $data = json_encode($value);

                                    echo '<tr>';
                                    echo '<td> <button class="btn btn-outline-primary " type="button"  onclick=\'receptionInput(' . $data . ')\'>' . $value['receipt_info']['receipt_processing_code'] . '</td>';
                                    echo '<td>' . $value['receipt_info']['uuid'] . '</td>';
                                    echo '<td>' . $value['receipt_info']['receipt_processing']['max_amount'] . '</td>';

                                    $expirationTime = intval($value['receipt_info']['receipt_processing']['expiration_time']);
                                    echo '<td>' . date('Y/m/d H:i:s', $expirationTime) . '</td>';
                                    echo '<td>' . $value['register']['name'] . '</td>';

                                    echo '</td>';
                                }

                                ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td><strong><strong>レシート番号</strong></strong></td>
                                    <td><strong>uuid</strong></td>
                                    <td><strong><strong>上限金額</strong><br></strong></td>
                                    <td><strong><strong>受付期限</strong><br></strong></td>
                                    <td><strong><strong>レジ</strong></strong></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="bg-white sticky-footer">
            <div class="container my-auto">
                <div class="text-center my-auto copyright"><span>Copyright © PLATFIELD INC.&nbsp;2022</span></div>
            </div>
        </footer>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/theme.js"></script>
<script src="assets/js/zectStudio_12H-Time-Format.js"></script>
</body>

</html>
