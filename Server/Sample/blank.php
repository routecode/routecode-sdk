<?php
require_once "processing.php";

$accessToken = getAccessToken();

echo "_POST:" . nl2br(print_r($_POST, true));

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title> <?php echo LABEL_BLANK . LABEL_HYPHEN . PRODUCT_NAME . LABEL_ENVIRONMENT; ?></title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Noto+Sans+Inscriptional+Pahlavi&amp;display=swap">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css">
    <script type="text/javascript" src="common.js"></script>
</head>

<body id="page-top">
<div id="wrapper">
    <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0">
        <div class="container-fluid d-flex flex-column p-0"><a
                    class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0"
                    href="index.php">
                <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-solar-panel"></i></div>
                <div class="sidebar-brand-text mx-3"><span>Route Pay</span></div>
            </a>
            <hr class="sidebar-divider my-0">
            <ul class="navbar-nav text-light" id="accordionSidebar">
                <li class="nav-item"></li>
                <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_ACCESS_TOKEN) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_ACCESS_TOKEN; ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"
                             stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                             stroke-linejoin="round" class="icon icon-tabler icon-tabler-login">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2"></path>
                            <path d="M20 12h-13l3 -3m0 6l-3 -3"></path>
                        </svg>
                        <span><?php echo LABEL_ACCESS_TOKEN; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_ACCESS_SELECT) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_ACCESS_SELECT; ?>"><i class="far fa-list-alt"></i><span><?php echo LABEL_SELECT; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_RECEPTION_LIST) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_RECEPTION_LIST; ?>"><i class="far fa-list-alt"></i><span><?php echo LABEL_RECEPTION_LIST; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_PAY_REFUND_LIST) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_PAY_REFUND_LIST; ?>"><i class="far fa-list-alt"></i><span><?php echo LABEL_PAY_REFUND_LIST; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_USER_LIST) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_USER_LIST; ?>"><i class="far fa-list-alt"></i><span><?php echo LABEL_USER_LIST; ?></span></a>
                    <a class="nav-link <?php echo strcmp($_SERVER['SCRIPT_NAME'] , '/'. URL_MY_CHANGE_PASSWORD) == 0 ? "active" : ""; ?>" href="<?php echo URL_MY_CHANGE_PASSWORD; ?>" ><i class="far fa-list-alt"></i><span><?php echo LABEL_USER_CHANGE_PASSWORD; ?></span></a>
                </li>
            </ul>
            <div class="text-center d-none d-md-inline">
                <button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button>
            </div>
        </div>
    </nav>
    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                <div class="container-fluid">
                    <button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i
                                class="fas fa-bars"></i></button>
                    <h1>
                        <span style="color: rgba(var(--bs-dark-rgb), var(--bs-text-opacity)) ; background-color: rgb(248, 249, 252);"><?php echo LABEL_ENVIRONMENT; ?></span><br>
                    </h1>
                </div>
            </nav>
            <div class="container-fluid">
                <h3 class="text-dark mb-1"><span
                            style="color: rgba(var(--bs-dark-rgb), var(--bs-text-opacity)) ;"> <?php echo LABEL_BLANK ; ?></span></h3>
            </div>
        </div>
        <footer class="bg-white sticky-footer">
            <div class="container my-auto">
                <div class="text-center my-auto copyright"><span>Copyright © PLATFIELD INC.&nbsp;2022</span></div>
            </div>
        </footer>
    </div>
    <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
</div>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/theme.js"></script>
<script src="assets/js/zectStudio_12H-Time-Format.js"></script>
</body>

</html>
