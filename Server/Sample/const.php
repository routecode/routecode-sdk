<?php
date_default_timezone_set('Asia/Tokyo');

const URL_TEST_SERVER_BASE = "https://stg-sandbox.unid.net";
const URL_BASE = "https://stg-pay.unid.net";

const LABEL_ENVIRONMENT = "＜検証環境＞";

const PRODUCT_NAME = "RoutePay";
const LABEL_HYPHEN = " - ";

const LABEL_404 = "Page Not Found ";
const LABEL_ACCESS_TOKEN = "アクセストークン発行";
const LABEL_BLANK = "Blank";
const LABEL_CALLBACK = "テストサイト（コールバック画面）";
const LABEL_GET_ACCESS_TOKEN = "アクセストークン取得";
const LABEL_INDEX = "はじめに";
const LABEL_PAY_PROCESSING_REFUND = "払い戻し";
const LABEL_PAY_REFUND_INPUT = "返金";
const LABEL_PAY_REFUND_LIST = "決済･返金リスト";
const LABEL_RECEPTION_INPUT = "決済処理確認";
const LABEL_RECEPTION_LIST = "受付リスト";
const LABEL_RECEPTION_PROCESSING_DELETE = "受付削除処理";
const LABEL_RECEPTION_PROCESSING_PAY = "決済処理";
const LABEL_SELECT = "受付・決済";
const LABEL_USER_DATA = "レシート履歴";
const LABEL_USER_LIST = "ユーザーリスト(履歴用)";
const LABEL_USER_CHANGE_PASSWORD = "パスワード変更";
const LABEL_USER_CHANGE_PASSWORD_PROCESSING = "パスワード変更処理";

const URL_ACCESS_TOKEN_DESTROY = URL_BASE . "/payment/access_token_destroy";
const URL_ACCESS_TOKEN = URL_BASE . "/payment/access_token";
const URL_STREAM_SEED = URL_BASE . "/payment/stream_seed";
const URL_PAY_REFUND = URL_BASE . "/payment/pay_refund";
const URL_PAY_LIST = URL_BASE . "/payment/pay_list";
const URL_PAY_RECEPTION_LIST = URL_BASE . "/payment/reception_list";
const URL_CHANGE_PASSWORD = URL_BASE . "/payment/change_password";

const URL_PAY_DELETE_RECEPTION = URL_BASE . "/payment/delete_reception";
const URL_PAY_RECEPTION_SETTLEMENT = URL_BASE . "/payment/reception_settlement";

const URL_MY_ACCESS_SELECT = "select.php";
const URL_MY_RECEPTION_LIST = "reception-list.php";
const URL_MY_PAY_REFUND_LIST = "pay-refund-list.php";
const URL_MY_ACCESS_TOKEN = "access-token.php";
const URL_MY_USER_LIST = "user-list.php";
const URL_MY_CHANGE_PASSWORD = "change-password.php";


const DEMO_SERVICE_ID = "";
const DEMO_MERCHANT_ID = "";
const DEMO_REGISTER_ID = "";
const DEMO_PASSWORD = "";


const DEMO_AMOUNT = 500;
const DEMO_MAX_AMOUNT = 6000;

