package com.platfield.unIDSDK.Sample.routecodetest;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.platfield.unidsdk.routecode.EnvironmentMode;
import com.platfield.unidsdk.routecode.RoutePay;
import com.platfield.unidsdk.routecode.utility.StringSerializer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {
    public static String UUID = "UUID";
    public static String USER_NO = "USER_NO";

    public static String USER_DATA_FILE_NAME = "userData";
    public static String USER_DATA_INTENT = "userData";

    //    public static  EnvironmentMode ENVIRONMENT_MODE = EnvironmentMode.Production;
    public static EnvironmentMode ENVIRONMENT_MODE = EnvironmentMode.Staging; // <-ステージングです。こちらでテストして下さい。

    public static String DNS_PRODUCTION = "https://sandbox.unid.net";
    public static String DNS_STAGING = "https://stg-sandbox.unid.net";

    public static String SERVICE_ID =  "サービスIDを入れてください";
    public static String MERCHANT_ID = "店舗IDを入れてください";

    private Activity m_activity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.m_activity = this;

        // カード登録
        Button uuidButton1 = findViewById(R.id.button1);
        uuidButton1.setOnClickListener(v -> {
            callCardRegister();

        });

        // 決済
        Button uuidButton2 = findViewById(R.id.button2);
        uuidButton2.setOnClickListener(v -> {
            callPayment();
        });

        Button uuidButton3 = findViewById(R.id.button3);
        uuidButton3.setOnClickListener(v -> {
            String userDataString = readFile(USER_DATA_FILE_NAME);

            if (userDataString.length() > 0) {
                Intent intent;
                intent = new Intent(this.getApplicationContext(), HistoryActivity.class);

                intent.putExtra(USER_DATA_INTENT, userDataString);
                this.startActivity(intent);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String userDataString = readFile("userData");

        if (userDataString.length() <= 0) {
            Intent intent;
            intent = new Intent(this.getApplicationContext(), UserNameActivity.class);
            this.startActivity(intent);
        }
    }

    public void fileSave(String fileName, String data) {
        try (FileOutputStream fileOutputstream = openFileOutput(fileName,
                Context.MODE_PRIVATE)) {

            fileOutputstream.write(data.getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readFile(String fileName) {
        String text = null;

        try (FileInputStream fileInputStream = openFileInput(fileName);
             BufferedReader reader = new BufferedReader(
                     new InputStreamReader(fileInputStream, StandardCharsets.UTF_8))) {

            String lineBuffer;
            while ((lineBuffer = reader.readLine()) != null) {
                text = lineBuffer;
            }

        } catch (IOException e) {
            text = "";
        }

        return text;
    }

    public interface CardInfoCallback {
        void success(UserData userData);

        void failed(int var1, String var2);
    }

    // ユーザー情報取得
    private void getUserInfo(CardInfoCallback callback) {
        String userDataString = readFile(USER_DATA_FILE_NAME);

        if (userDataString.length() > 0) {
            UserData userData= (UserData) StringSerializer.deserialize(userDataString);
            callback.success(userData);
        } else {
            Intent intent;
            intent = new Intent(this.getApplicationContext(), UserNameActivity.class);
            this.startActivity(intent);
        }
    }

    // カード登録
    private void callCardRegister() {
        getUserInfo(new CardInfoCallback() {
            @Override
            public void success(UserData userData) {
                RoutePay routePay = new RoutePay();
                routePay.callCardRegister(
                        userData.getUuid(),
                        userData.getUserNo(),
                        m_activity,
                        ENVIRONMENT_MODE,
                        new RoutePay.ResponseCardRegistCallback() {
                            @Override
                            public void success(String s, int i) {
                                Toast.makeText(getApplicationContext(), "カード登録完了", Toast.LENGTH_LONG).show();
                            }
                            @Override
                            public void failed(int i, String s) {
                                Toast.makeText(getApplicationContext(), "ユーザー情報取得失敗", Toast.LENGTH_LONG).show();
                            }
                        }
                );
            }

            @Override
            public void failed(int var1, String var2) {
                Toast.makeText(getApplicationContext(), "ユーザー情報取得失敗", Toast.LENGTH_LONG).show();
            }
        });
    }

    // 決済
    private void callPayment() {
        getUserInfo(new CardInfoCallback() {
            @Override
            public void success(UserData userData) {
                RoutePay routePay = new RoutePay();

                routePay.callPayment(
                        userData.getUuid(),
                        userData.getUserNo(),
                        m_activity,
                        ENVIRONMENT_MODE,
                        new RoutePay.ResponsePaymentCallback(){
                            @Override
                            public void success(String s, int i) {
                                Toast.makeText(getApplicationContext(), "決済完了", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void failed(int i, String s) {
                                Toast.makeText(getApplicationContext(), "失敗", Toast.LENGTH_LONG).show();
                            }
                        }
                );
            }

            @Override
            public void failed(int var1, String var2) {
                Toast.makeText(getApplicationContext(), "ユーザー情報取得失敗", Toast.LENGTH_LONG).show();
            }
        });
    }
}


