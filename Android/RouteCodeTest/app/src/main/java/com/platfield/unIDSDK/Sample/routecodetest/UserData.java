package com.platfield.unIDSDK.Sample.routecodetest;

import java.io.Serializable;

public class UserData  implements Serializable {
    private String uuid = null;
    private Integer userNo = 0;

    public UserData( String uuid, Integer userNo) {
        this.uuid = uuid;
        this.userNo = userNo;
    }

    public String getUuid() {
        return uuid;
    }
    public Integer getUserNo() {
        return userNo;
    }
}
