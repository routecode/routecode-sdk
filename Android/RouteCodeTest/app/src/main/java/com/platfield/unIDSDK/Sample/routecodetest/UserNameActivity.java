package com.platfield.unIDSDK.Sample.routecodetest;

import static com.platfield.unIDSDK.Sample.routecodetest.MainActivity.USER_DATA_FILE_NAME;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.platfield.unidsdk.routecode.RoutePay;
import com.platfield.unidsdk.routecode.utility.StringSerializer;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class UserNameActivity extends AppCompatActivity {
    private Activity m_activity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_name);

        m_activity = this;

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        String dateToStr = dateFormat.format(date);
        EditText editText = findViewById(R.id.editTextTextPersonName);

        editText.setText("ユーザー" + dateToStr, TextView.BufferType.NORMAL);

        Button button5 = findViewById(R.id.buttonNameSetting);

        button5.setOnClickListener(v -> {
            registerUser();
        });

    }

    private void registerUser() {
        RoutePay routePay = new RoutePay();
        routePay.initialUuid(
                MainActivity.SERVICE_ID,
                MainActivity.MERCHANT_ID,
                this,
                MainActivity.ENVIRONMENT_MODE,
                new RoutePay.ResponseInitialUuidCallback(){

                    @Override
                    public void success(String uuidString, int userNo) {
                        // データ保存
                        UserData userData = new UserData(uuidString,userNo);
                        String userDataString = StringSerializer.serialize(userData);

                        fileSave(USER_DATA_FILE_NAME, userDataString);
                        EditText editText = findViewById(R.id.editTextTextPersonName);

                        SpannableStringBuilder sb = (SpannableStringBuilder) editText.getText();
                        String name = sb.toString();

                        Thread thread = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    PostUserName(name, uuidString);
                                    finish();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        thread.start();

                    }

                    @Override
                    public void failed(int i, String s) {
//                        Toast.makeText(m_activity,"initialUuid:error2:" + s ,Toast.LENGTH_LONG).show();

                    }
                }
        );
    }

    public String PostUserName(String name, String uuid) {
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        String result = "";
        StringBuilder str = new StringBuilder();

        try {
            String dns = null;

            switch (MainActivity.ENVIRONMENT_MODE) {
                case Production:
                    dns = MainActivity.DNS_PRODUCTION;
                    break;
                case Staging:
                    dns = MainActivity.DNS_STAGING;
                    break;
                case Develop:
                    dns = MainActivity.DNS_DEVELOP;
                    break;
            }

            URL url = new URL(dns + "/get_receipt_data.php?uuid=" + uuid);
            // 接続先URLへのコネクションを開く．まだ接続されていない
            urlConnection = (HttpURLConnection) url.openConnection();
            // 接続タイムアウトを設定
            urlConnection.setConnectTimeout(10000);
            // レスポンスデータの読み取りタイムアウトを設定
            urlConnection.setReadTimeout(10000);
            // ヘッダーにUser-Agentを設定
            urlConnection.addRequestProperty("User-Agent", "Android");
            // ヘッダーにAccept-Languageを設定
            urlConnection.addRequestProperty("Accept-Language", Locale.getDefault().toString());
            //ヘッダーにContent-Typeを設定する
            urlConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // HTTPメソッドを指定
            urlConnection.setRequestMethod("POST");

            urlConnection.setUseCaches(false);
            //リクエストボディの送信を許可しない
            urlConnection.setDoOutput(true);
            //レスポンスボディの受信を許可する
            urlConnection.setDoInput(true);
            // 通信開始
            urlConnection.connect();


            //ステップ5:リクエストボディの書き出しを行う。
            OutputStream outputStream = urlConnection.getOutputStream();
            Map<String, String> keyValues = new HashMap<>();

            keyValues.put("uuid", uuid);
            keyValues.put("name", name);


            if (keyValues.size() > 0) {
                Uri.Builder builder = new Uri.Builder();
                //HashMapを[key=value]形式の文字列に変換する
                for (String key : keyValues.keySet()) {
                    //[key=value]形式の文字列に変換する。
                    builder.appendQueryParameter(key, keyValues.get(key));
                }
                //[key=value&key=value…]形式の文字列に変換する。
                String join = builder.build().getEncodedQuery();
                PrintStream ps = new PrintStream(outputStream);
                ps.print(join);
                ps.close();
            }
            outputStream.close();

            // レスポンスコードを取得
            int statusCode = urlConnection.getResponseCode();
            // レスポンスコード200は通信に成功したことを表す
            if (statusCode == 200) {
                inputStream = urlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                // 1行ずつレスポンス結果を取得しstrに追記
                result = bufferedReader.readLine();
                while (result != null) {
                    str.append(result);
                    result = bufferedReader.readLine();
                }
                bufferedReader.close();
            }
        } catch (MalformedURLException e) {
//            Toast.makeText(m_activity,"PostUserName:error1:" +  e.toString()  ,Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } catch (IOException e) {
//            Toast.makeText(m_activity,"PostUserName:error2:" +  e.toString()  ,Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        // レスポンス結果のJSONをString型で返す
        return str.toString();
    }


    public void fileSave(String fileName, String data) {
        try (FileOutputStream fileOutputstream = openFileOutput(fileName,
                Context.MODE_PRIVATE)) {

            fileOutputstream.write(data.getBytes());

        } catch (IOException e) {
            Toast.makeText(m_activity,"fileSave:error1:" +  e.toString()  ,Toast.LENGTH_LONG).show();
//            e.printStackTrace();
        }
    }
}