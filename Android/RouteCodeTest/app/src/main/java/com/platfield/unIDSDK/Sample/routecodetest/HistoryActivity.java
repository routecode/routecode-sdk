package com.platfield.unIDSDK.Sample.routecodetest;

import static com.platfield.unIDSDK.Sample.routecodetest.MainActivity.USER_DATA_INTENT;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.widget.TextView;

import com.platfield.unidsdk.routecode.utility.StringSerializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class HistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        TextView textView = findViewById(R.id.textViewHistory);
        textView.setMovementMethod(new ScrollingMovementMethod());

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String userInfoString = getIntent().getStringExtra(USER_DATA_INTENT);
                    UserData userData = (UserData) StringSerializer.deserialize(userInfoString);

                    String textDataBase64 = getHistoryData(userData);
                    String textData = new String(Base64.decode(textDataBase64, Base64.DEFAULT));

                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(textData);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    public String getHistoryData(UserData userData) {
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        String result = "";
        String str = "";
        try {
            String dns = null;

            switch (MainActivity.ENVIRONMENT_MODE) {
                case Production:
                    dns = MainActivity.DNS_PRODUCTION;
                    break;
                case Staging:
                    dns = MainActivity.DNS_STAGING;
                    break;
                case Develop:
                    dns = MainActivity.DNS_DEVELOP;
                    break;
            }

            URL url = new URL(dns + "/get-receipt-data.php?uuid=" + userData.getUuid());
            // 接続先URLへのコネクションを開く．まだ接続されていない
            urlConnection = (HttpURLConnection) url.openConnection();
            // 接続タイムアウトを設定
            urlConnection.setConnectTimeout(10000);
            // レスポンスデータの読み取りタイムアウトを設定
            urlConnection.setReadTimeout(10000);
            // ヘッダーにUser-Agentを設定
            urlConnection.addRequestProperty("User-Agent", "Android");
            // ヘッダーにAccept-Languageを設定
            urlConnection.addRequestProperty("Accept-Language", Locale.getDefault().toString());
            // HTTPメソッドを指定
            urlConnection.setRequestMethod("GET");
            //リクエストボディの送信を許可しない
            urlConnection.setDoOutput(false);
            //レスポンスボディの受信を許可する
            urlConnection.setDoInput(true);
            // 通信開始
            urlConnection.connect();
            // レスポンスコードを取得
            int statusCode = urlConnection.getResponseCode();
            // レスポンスコード200は通信に成功したことを表す
            if (statusCode == 200) {
                inputStream = urlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
                // 1行ずつレスポンス結果を取得しstrに追記
                result = bufferedReader.readLine();
                while (result != null) {
                    str += result;
                    result = bufferedReader.readLine();
                }
                bufferedReader.close();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // レスポンス結果のJSONをString型で返す
        return str;
    }
}