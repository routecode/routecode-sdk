# RouteCode sdk for Android

## フォルダ構成

```
.
├── README.md
├── License                 <- ライセンス表記用テキスト群
├── RouteCode-release.aar   <- RouteCode　SDKです。
├── RouteCodeTest           <- Java のサンプルアプリソース
└── RouteCodeKotlinTest     <- Kotlin のサンプルアプリソース
```

## SDK 本体

「RouteCode-release.aar」が本体となります。

### フレームワークの追加方法

1. ライブラリの設定 (root)/app/libs フォルダに「RouteCode-release.aar」をおく

2.アプリケーション Gradle のビルド依存関係にライブラリを指定します。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar','*.aar'])
    ・・・
```

3. アプリケーション Gradle のビルド依存関係に QR コード用のカメラ・バーコードライブラリを追加します。

```
dependencies {
...
implementation 'com.google.mlkit:barcode-scanning:17.0.2'
implementation 'androidx.camera:camera-camera2:1.2.0-alpha01'
implementation 'androidx.camera:camera-lifecycle:1.2.0-alpha01'
implementation 'androidx.camera:camera-view:1.2.0-alpha01'
...
```

4. アプリケーション内でクラスおよびメソッドを呼び出してください。

## サンプルアプリ

### Java

「RouteCodeSwiftTest」に同梱しております。

### Kotlin

「RouteCodeKotlinTest」に同梱しております。

## ライセンスについて

本 SDK では、オープンソースを使用しております。

「License」内に置いているテキストは、本件で使用しているオープンソースの、ライセンス表記となります。

### OpenSSL(1.1.1)

https://www.openssl.org/
