package com.platfield.unIDSDK.Sample.RouteCodeKotlinTest

import android.net.Uri
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.platfield.unIDSDK.Sample.RouteCodeKotlinTest.MainActivity.Companion.USER_DATA_FILE_NAME
import com.platfield.unidsdk.routecode.EnvironmentMode
import com.platfield.unidsdk.routecode.RoutePay
import com.platfield.unidsdk.routecode.RoutePay.ResponseInitialUuidCallback
import com.platfield.unidsdk.routecode.utility.General
import com.platfield.unidsdk.routecode.utility.StringSerializer
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.StandardCharsets
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class UserNameActivity : AppCompatActivity() {
    var m_userNameActivity: UserNameActivity = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_name)

        val dateFormat: DateFormat = SimpleDateFormat("yyyyMMddHHmmss")
        val date = Date()
        val dateToStr = dateFormat.format(date)
        val editText = findViewById<EditText>(R.id.editTextTextPersonName)

        editText.setText("ユーザー$dateToStr", TextView.BufferType.NORMAL)

        m_userNameActivity = this

        val buttonRegisterUser = findViewById<Button>(R.id.buttonNameSetting)

        buttonRegisterUser.setOnClickListener { v: View? -> registerUser() }

    }

    private fun registerUser() {
        val routePay = RoutePay()
        routePay.initialUuid(
            MainActivity.SERVICE_ID,
            MainActivity.MERCHANT_ID,
            m_userNameActivity,
            MainActivity.ENVIRONMENT_MODE,
            object : ResponseInitialUuidCallback {
                override fun success(uuid: String?, userNo: Int) {
                    // データ保存
                    val userData = UserData(uuid, userNo)
                    val userDataString = StringSerializer.serialize(userData)
                    fileSave(USER_DATA_FILE_NAME, userDataString)

                    val editText = findViewById<EditText>(R.id.editTextTextPersonName)
                    val sb = editText.text as SpannableStringBuilder
                    val name = sb.toString()
                    if (uuid != null) {
                        val thread = Thread {
                            try {
                                PostUserName(name, uuid)
                                m_userNameActivity.finish()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                        thread.start()
                    } else {
                        return
                    }
                }

                override fun failed(status: Int, message: String?) {
                    General.log(message)
                }
            }
        )
    }

    fun PostUserName(name: String, uuid: String): String? {
        var urlConnection: HttpURLConnection? = null
        var inputStream: InputStream? = null
        var result = ""
        val str = StringBuilder()
        try {
            val dns: String = when (MainActivity.ENVIRONMENT_MODE) {
                EnvironmentMode.Production -> MainActivity.DNS_PRODUCTION
                EnvironmentMode.Staging -> MainActivity.DNS_STAGING
                EnvironmentMode.Develop -> MainActivity.DNS_DEVELOP
            }

            val url = URL("$dns/get_receipt_data.php?uuid=$uuid")
            // 接続先URLへのコネクションを開く．まだ接続されていない
            urlConnection = url.openConnection() as HttpURLConnection
            // 接続タイムアウトを設定
            urlConnection.connectTimeout = 10000
            // レスポンスデータの読み取りタイムアウトを設定
            urlConnection!!.readTimeout = 10000
            // ヘッダーにUser-Agentを設定
            urlConnection.addRequestProperty("User-Agent", "Android")
            // ヘッダーにAccept-Languageを設定
            urlConnection.addRequestProperty("Accept-Language", Locale.getDefault().toString())
            //ヘッダーにContent-Typeを設定する
            urlConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")

            // HTTPメソッドを指定
            urlConnection.requestMethod = "POST"
            urlConnection.useCaches = false
            //リクエストボディの送信を許可しない
            urlConnection.doOutput = true
            //レスポンスボディの受信を許可する
            urlConnection.doInput = true
            // 通信開始
            urlConnection.connect()


            //ステップ5:リクエストボディの書き出しを行う。
            val outputStream = urlConnection.outputStream
            val keyValues: MutableMap<String, String> = HashMap()
            keyValues["uuid"] = uuid
            keyValues["name"] = name
            if (keyValues.size > 0) {
                val builder = Uri.Builder()
                //HashMapを[key=value]形式の文字列に変換する
                for (key in keyValues.keys) {
                    //[key=value]形式の文字列に変換する。
                    builder.appendQueryParameter(key, keyValues[key])
                }
                //[key=value&key=value…]形式の文字列に変換する。
                val join = builder.build().encodedQuery
                val ps = PrintStream(outputStream)
                ps.print(join)
                ps.close()
            }
            outputStream.close()

            // レスポンスコードを取得
            val statusCode = urlConnection.responseCode
            // レスポンスコード200は通信に成功したことを表す
            if (statusCode == 200) {
//                inputStream = urlConnection.inputStream
//                val bufferedReader =
//                    BufferedReader(InputStreamReader(inputStream, StandardCharsets.UTF_8))
//                // 1行ずつレスポンス結果を取得しstrに追記
//                result = bufferedReader.readLine()
//                while (result != null) {
//                    str.append(result)
//                    result = bufferedReader.readLine()
//                }
//                bufferedReader.close()
            }
        } catch (e: MalformedURLException) {
//            Toast.makeText(m_activity,"PostUserName:error1:" +  e.toString()  ,Toast.LENGTH_LONG).show();
            e.printStackTrace()
        } catch (e: IOException) {
//            Toast.makeText(m_activity,"PostUserName:error2:" +  e.toString()  ,Toast.LENGTH_LONG).show();
            e.printStackTrace()
        }
        // レスポンス結果のJSONをString型で返す
        return str.toString()
    }

    fun PostUserName2(name: String, uuid: String): String? {
        var urlConnection: HttpURLConnection? = null
        val str = StringBuilder()
        try {
            val dns: String = when (MainActivity.ENVIRONMENT_MODE) {
                EnvironmentMode.Production -> MainActivity.DNS_PRODUCTION
                EnvironmentMode.Staging -> MainActivity.DNS_STAGING
                EnvironmentMode.Develop -> MainActivity.DNS_DEVELOP
            }

            val url = URL("$dns/get_receipt_data.php?uuid=$uuid")
            // 接続先URLへのコネクションを開く．まだ接続されていない
            urlConnection = url.openConnection() as HttpURLConnection
            // 接続タイムアウトを設定
            urlConnection.connectTimeout = 10000
            // レスポンスデータの読み取りタイムアウトを設定
            urlConnection!!.readTimeout = 10000
            // ヘッダーにUser-Agentを設定
            urlConnection.addRequestProperty("User-Agent", "Android")
            // ヘッダーにAccept-Languageを設定
            urlConnection.addRequestProperty("Accept-Language", Locale.getDefault().toString())
            //ヘッダーにContent-Typeを設定する
            urlConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            // HTTPメソッドを指定
            urlConnection.requestMethod = "POST"
            urlConnection.useCaches = false
            //リクエストボディの送信を許可しない
            urlConnection.doOutput = true
            //レスポンスボディの受信を許可する
            urlConnection.doInput = true
            // 通信開始
            urlConnection.connect()


            //ステップ5:リクエストボディの書き出しを行う。
            val outputStream = urlConnection.outputStream
            val keyValues: MutableMap<String, String> = HashMap()
            keyValues["uuid"] = uuid
            keyValues["name"] = name
            if (keyValues.size > 0) {
                val builder = Uri.Builder()
                //HashMapを[key=value]形式の文字列に変換する
                for (key in keyValues.keys) {
                    //[key=value]形式の文字列に変換する。
                    builder.appendQueryParameter(key, keyValues[key])
                }
                //[key=value&key=value…]形式の文字列に変換する。
                val join = builder.build().encodedQuery
                val ps = PrintStream(outputStream)
                ps.print(join)
                ps.close()
            }
            outputStream.close()

            // レスポンスコードを取得
            val statusCode = urlConnection.responseCode
            // レスポンスコード200は通信に成功したことを表す
            if (statusCode == 200) {
            }
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        // レスポンス結果のJSONをString型で返す
        return str.toString()
    }

    fun fileSave(fileName: String?, data: String) {
        try {
            openFileOutput(
                fileName,
                MODE_PRIVATE
            ).use { fileOutputstream -> fileOutputstream.write(data.toByteArray()) }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}