package com.platfield.unIDSDK.Sample.RouteCodeKotlinTest

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.platfield.unidsdk.routecode.EnvironmentMode
import com.platfield.unidsdk.routecode.RoutePay
import com.platfield.unidsdk.routecode.RoutePay.*
import com.platfield.unidsdk.routecode.utility.General
import com.platfield.unidsdk.routecode.utility.StringSerializer
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets

class MainActivity : AppCompatActivity() {
    companion object {

        var USER_DATA_FILE_NAME = "userData"
        var USER_DATA_INTENT = "userData"

        //  val ENVIRONMENT_MODE = EnvironmentMode.Production
        var ENVIRONMENT_MODE = EnvironmentMode.Staging // <-ステージングです。こちらでテストして下さい。

        var DNS_PRODUCTION = "https://sandbox.unid.net"
        var DNS_STAGING = "https://stg-sandbox.unid.net"

        var SERVICE_ID = "サービスIDを入れてください"
        var MERCHANT_ID = "店舗IDを入れてください"
    }

    private var m_activity: Activity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        m_activity = this

        // カード登録
        val uuidButton1 = findViewById<Button>(R.id.button1)
        uuidButton1.setOnClickListener { v: View? -> callCardRegister() }

        // 決済
        val uuidButton2 = findViewById<Button>(R.id.button2)
        uuidButton2.setOnClickListener { v: View? -> callPayment() }

        // ユーザー情報取得
        val uuidButton3 = findViewById<Button>(R.id.button3)
        uuidButton3.setOnClickListener { v: View? ->
            val userDataString = readFile(USER_DATA_FILE_NAME)

            if (userDataString!!.length > 0) {
                val intent: Intent
                intent = Intent(this.applicationContext, HistoryActivity::class.java)
                intent.putExtra(USER_DATA_INTENT, userDataString)
                this.startActivity(intent)
            }
        }

    }

    override fun onResume() {
        super.onResume()

        val userInfoString = readFile(USER_DATA_FILE_NAME)

        if (userInfoString!!.length <= 0) {
            General.log("")
            val intent: Intent
            intent = Intent(this.applicationContext, UserNameActivity::class.java)
            this.startActivity(intent)
        }
    }


    private fun fileSave(fileName: String, data: String) {
        try {
            openFileOutput(
                fileName,
                MODE_PRIVATE
            ).use { fileOutputstream -> fileOutputstream.write(data.toByteArray()) }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun readFile(fileName: String): String? {
        var text: String? = null
        try {
            openFileInput(fileName).use { fileInputStream ->
                BufferedReader(
                    InputStreamReader(fileInputStream, StandardCharsets.UTF_8)
                ).use { reader ->
                    var lineBuffer: String?
                    while (reader.readLine().also { lineBuffer = it } != null) {
                        text = lineBuffer
                    }
                }
            }
        } catch (e: IOException) {
            text = ""
        }
        return text
    }

    interface CardInfoCallback {
        fun success(userData: UserData?)
        fun failed(var1: Int, var2: String?)
    }

    // ユーザー情報取得
    private fun getUserInfo(callback: CardInfoCallback) {
        val userDataString = readFile(USER_DATA_FILE_NAME)
        if (userDataString!!.length > 0) {
            val userData = StringSerializer.deserialize(userDataString) as UserData
            callback.success(userData)
        } else {
            val intent: Intent
            intent = Intent(this.applicationContext, UserNameActivity::class.java)
            this.startActivity(intent)
        }
    }

    // カード登録
    private fun callCardRegister() {
        getUserInfo(object : CardInfoCallback {
            override fun success(userData: UserData?) {
                val routePay = RoutePay()
                if (userData != null) {
                    routePay.callCardRegister(
                        userData.uuid,
                        userData.userNo,
                        m_activity,
                        ENVIRONMENT_MODE,
                        object : ResponseCardRegistCallback {
                            override fun success(s: String, i: Int) {
                                Toast.makeText(applicationContext, "カード登録完了", Toast.LENGTH_LONG).show()
                            }

                            override fun failed(i: Int, s: String) {
                                Toast.makeText(applicationContext, "ユーザー情報取得失敗", Toast.LENGTH_LONG)
                                    .show()
                            }
                        }
                    )
                }
            }

            override fun failed(var1: Int, var2: String?) {
                Toast.makeText(applicationContext, "ユーザー情報取得失敗", Toast.LENGTH_LONG).show()
            }
        })
    }

    // 決済
    private fun callPayment() {
        getUserInfo(object : CardInfoCallback {
            override fun success(userData: UserData?) {
                val routePay = RoutePay()
                if (userData != null) {
                    routePay.callPayment(
                        userData.uuid,
                        userData.userNo,
                        m_activity,
                        ENVIRONMENT_MODE,
                        object : ResponsePaymentCallback {
                            override fun success(s: String, i: Int) {
                                Toast.makeText(applicationContext, "決済完了", Toast.LENGTH_LONG).show()
                            }

                            override fun failed(i: Int, s: String) {
                                Toast.makeText(applicationContext, "失敗", Toast.LENGTH_LONG).show()
                            }
                        }
                    )
                }
            }

            override fun failed(var1: Int, var2: String?) {
                Toast.makeText(applicationContext, "ユーザー情報取得失敗", Toast.LENGTH_LONG).show()
            }
        })
    }
}

