package com.platfield.unIDSDK.Sample.RouteCodeKotlinTest

import java.io.Serializable

class UserData(uuid: String?, userNo: Int) : Serializable {
    var uuid: String? = null
    var userNo = 0

    init {
        this.uuid = uuid
        this.userNo = userNo
    }
}
