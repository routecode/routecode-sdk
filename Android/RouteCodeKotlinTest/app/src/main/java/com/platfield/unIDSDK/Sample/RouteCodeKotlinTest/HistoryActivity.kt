package com.platfield.unIDSDK.Sample.RouteCodeKotlinTest

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Base64
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.platfield.unIDSDK.Sample.RouteCodeKotlinTest.MainActivity.Companion.USER_DATA_INTENT
import com.platfield.unidsdk.routecode.EnvironmentMode
import com.platfield.unidsdk.routecode.utility.StringSerializer
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*

class HistoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        val textView = findViewById<TextView>(R.id.textViewHistory)
        textView.movementMethod = ScrollingMovementMethod()
        val thread = Thread {
            try {
                val userInfoString = intent.getStringExtra(USER_DATA_INTENT)
                val userData = StringSerializer.deserialize(userInfoString) as UserData
                val textDataBase64 = getHistoryData(userData)
                val textData =
                    String(Base64.decode(textDataBase64, Base64.DEFAULT))
                textView.post { textView.text = textData }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        thread.start()
    }

    fun getHistoryData(uuid: UserData): String? {
        var urlConnection: HttpURLConnection? = null
        var str = ""
        try {
            val dns: String = when (MainActivity.ENVIRONMENT_MODE) {
                EnvironmentMode.Production -> MainActivity.DNS_PRODUCTION
                EnvironmentMode.Staging -> MainActivity.DNS_STAGING
                EnvironmentMode.Develop -> MainActivity.DNS_DEVELOP
            }

            val url = URL("$dns/get_receipt_data.php?uuid=$uuid")

            // 接続先URLへのコネクションを開く．まだ接続されていない
            urlConnection = url.openConnection() as HttpURLConnection
            // 接続タイムアウトを設定
            urlConnection.connectTimeout = 10000
            // レスポンスデータの読み取りタイムアウトを設定
            urlConnection.readTimeout = 10000
            // ヘッダーにUser-Agentを設定
            urlConnection.addRequestProperty("User-Agent", "Android")
            // ヘッダーにAccept-Languageを設定
            urlConnection.addRequestProperty("Accept-Language", Locale.getDefault().toString())

            urlConnection.setRequestProperty("Content-type", "application/json; charset=utf-8")

            // HTTPメソッドを指定
            urlConnection.requestMethod = "GET"
            //リクエストボディの送信を許可しない
            urlConnection.doOutput = false
            //レスポンスボディの受信を許可する
            urlConnection.doInput = true
            // 通信開始
            urlConnection.connect()
            // レスポンスコードを取得
            val statusCode = urlConnection.responseCode
            // レスポンスコード200は通信に成功したことを表す
            if (statusCode == HttpURLConnection.HTTP_OK) {

                BufferedReader(InputStreamReader(urlConnection.getInputStream())).use { inp ->
                    var line: String?
                    while (inp.readLine().also { line = it } != null) {
                        // 1行ずつレスポンス結果を取得しstrに追記
                        str += line
                    }
                }
            }
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        // レスポンス結果のJSONをString型で返す
        return str
    }

}